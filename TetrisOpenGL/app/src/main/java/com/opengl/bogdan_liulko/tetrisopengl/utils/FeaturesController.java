package com.opengl.bogdan_liulko.tetrisopengl.utils;

import android.content.Context;

import com.opengl.bogdan_liulko.tetrisopengl.enums.EFeature;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.IRedrawCallback;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.Feature;
import com.opengl.bogdan_liulko.tetrisopengl.game.GameTimer;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.IFeatureListener;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.ITickListener;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.AShape;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.Cube;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.figures.PlacedFigure;

import java.util.ArrayList;

public class FeaturesController implements ITickListener{

    private final static int MAX_TIME = 30000;//30 seconds
    private final static int MIN_TIME = MAX_TIME / 2;//15 seconds
    private final static int TIME_TO_LIVE = MIN_TIME / 2;//7,5 seconds

    private int timeToBoost;

    private ArrayList<Feature> features;
    private IFeatureListener featureListener;
    private IRedrawCallback redrawCallback;
    private PlacedFigure placedFigure;
    private CameraController cameraController;
    private Context context;
    private ArrayList<AShape> shapes;

    public FeaturesController(Context context, CameraController cameraController, ArrayList<AShape> shapes, PlacedFigure placedFigure, IFeatureListener featureListener, IRedrawCallback redrawCallback) {
        this.cameraController = cameraController;
        this.featureListener = featureListener;
        this.redrawCallback = redrawCallback;
        this.placedFigure = placedFigure;
        this.context = context;
        this.shapes = shapes;
        features = new ArrayList<>();
        resetCounter();
    }

    @Override
    public void onTick() {
        checkAliveBoosts();
        timeToBoost -= GameTimer.ITERATION;
        checkTimeToBoost();
    }

    public void addPoints(int points) {
        timeToBoost -= points;
        checkTimeToBoost();
    }

    public void rowsRemoved(ArrayList<Integer> removedRows) {
        checkBoostInRows(removedRows);
        checkBoostMove(removedRows);
    }

    public void resetCounter() {
        timeToBoost = getRandomTime();
        shapes.removeAll(features);
        features.clear();
    }

    private void addBoostToField() {
        if (placedFigure.getPlacedCubes().size() == 0)
            return;
        timeToBoost = getRandomTime();
        Cube cube = getCubeForBoost();
        Feature feature = new Feature(context, cameraController, getRandomFeature(), cube.getCurrentRow(), cube.getCurrentColumn(), TIME_TO_LIVE);
        features.add(feature);
        shapes.add(feature);
        redrawCallback.redraw();
    }

    private EFeature getRandomFeature() {
        return EFeature.getFeatureById(getRandomFeatureId());
    }

    private int getRandomFeatureId() {
        return Utils.getRandomIntInRange(0, EFeature.values().length - 1);
    }

    private int getRandomTime() {
        return Utils.getRandomIntInRange(MIN_TIME, MAX_TIME);
    }

    private void checkTimeToBoost() {
        if (timeToBoost <= 0) {
            addBoostToField();
        }
    }

    private Cube getCubeForBoost() {
        ArrayList<Cube> cubes = placedFigure.getPlacedCubes();
        int index = Utils.getRandomIntInRange(0, cubes.size() - 1);
        return cubes.get(index);
    }

    private void checkAliveBoosts() {
        Feature feature;
        for (int i = 0; i < features.size(); i++) {
            feature = features.get(i);
            if (feature.onTimeTick() <= 0) {
                featureExpired(feature);
            }
        }
    }

    private void featureExpired(Feature expiredFeature) {
        features.remove(expiredFeature);
        shapes.remove(expiredFeature);
    }

    private ArrayList<Feature> checkBoostInRows(ArrayList<Integer> removedRows) {
        ArrayList<Feature> enabledFeatures = new ArrayList<>();
        Feature feature;
        int removedRow;
        for (int i = 0; i < features.size(); i++) {
            feature = features.get(i);
            for (int j = 0; j < removedRows.size(); j++) {
                removedRow = removedRows.get(j);
                if (feature.getRow() == removedRow) {
                    enabledFeatures.add(feature);
                    features.remove(feature);
                    featureListener.onFeatureEnable(feature);
                    shapes.remove(feature);
                }
            }
        }
        return enabledFeatures;
    }

    private void checkBoostMove(ArrayList<Integer> removedRows) {
        Feature feature;
        int removedRow;
        int countOfRemovedRows;
        for (int i = 0; i < features.size(); i++) {
            feature = features.get(i);
            countOfRemovedRows = 0;
            for (int j = 0; j < removedRows.size(); j++) {
                removedRow = removedRows.get(j);
                if (feature.getRow() > removedRow) {
                    countOfRemovedRows++;
                }
            }
            if (countOfRemovedRows > 0)
                feature.moveToRow(feature.getRow() - countOfRemovedRows);
        }
    }

}
