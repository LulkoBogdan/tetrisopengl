package com.opengl.bogdan_liulko.tetrisopengl.enums;

public enum EAction {
    MOVE_LEFT, MOVE_RIGHT, DROP, ROTATE, UP;

    public EAction getInverseAction() {
        if (this == MOVE_LEFT)
            return MOVE_RIGHT;
        if (this == MOVE_RIGHT)
            return MOVE_LEFT;
        return this;
    }
}
