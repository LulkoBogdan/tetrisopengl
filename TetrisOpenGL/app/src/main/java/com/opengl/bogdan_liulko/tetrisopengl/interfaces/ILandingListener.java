package com.opengl.bogdan_liulko.tetrisopengl.interfaces;

/**
 * Created by bogdan_liulko on 16.03.16.
 */
public interface ILandingListener {
    void figureLanded();
}
