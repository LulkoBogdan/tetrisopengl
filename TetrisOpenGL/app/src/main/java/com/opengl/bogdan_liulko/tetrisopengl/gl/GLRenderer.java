package com.opengl.bogdan_liulko.tetrisopengl.gl;

import android.content.Context;

import com.opengl.bogdan_liulko.tetrisopengl.enums.EAction;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.IRedrawCallback;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.IUIActionsListener;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.AShape;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.Axes;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.figures.SkyBox;
import com.opengl.bogdan_liulko.tetrisopengl.ui.views.StartDialog;
import com.opengl.bogdan_liulko.tetrisopengl.utils.CameraController;
import com.opengl.bogdan_liulko.tetrisopengl.utils.GameController;
import com.opengl.bogdan_liulko.tetrisopengl.utils.ScoreController;

import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class GLRenderer extends BaseGLRenderer implements IUIActionsListener{

    private final CameraController cameraController;

    private ScoreController scoreController;
    private StartDialog startDialog;
    private IRedrawCallback redrawCallback;
    private GameController gameController;
    private Context context;
    private ArrayList<AShape> shapes;

    public GLRenderer(Context context, IRedrawCallback redrawCallback) {
        this.context = context;
        this.redrawCallback = redrawCallback;
        shapes = new ArrayList<>();
        cameraController = new CameraController();
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        super.onSurfaceCreated(gl, config);
        if (gameController == null)
            gameController = new GameController(context, cameraController, shapes, redrawCallback, scoreController, startDialog);
        Axes axes = new Axes(context, cameraController);
        shapes.add(axes);
        SkyBox skyBox = new SkyBox(context, cameraController);
        shapes.add(skyBox);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl, width, height);
        bindMatrix(width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        super.onDrawFrame(gl);
        drawShapes();
    }

    public void setAction(EAction action) {
        gameController.setAction(action);
    }

    public void stopGame() {
        gameController.stopGame();
    }

    private void drawShapes() {
        for (int i = 0; i < shapes.size(); i++) {
            shapes.get(i).draw();
        }
    }

    public void setScoreController(ScoreController scoreController) {
        this.scoreController = scoreController;
    }

    public void setStartDialog(StartDialog startDialog) {
        this.startDialog = startDialog;
    }

    private void bindMatrix(int width, int height) {
        for (AShape shape : shapes) {
            shape.bindMatrix(width, height);
        }

    }

    @Override
    public boolean onBackPressed() {
        return gameController.isRunning();
    }

    @Override
    public void onPauseClick() {
        gameController.pauseGame();
    }

    @Override
    public void onStartClick() {
        gameController.startGame();
    }

    @Override
    public void onStopClick() {
        gameController.stopGame();
    }
}
