package com.opengl.bogdan_liulko.tetrisopengl.shapes.figures;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.opengl.bogdan_liulko.tetrisopengl.enums.EFigure;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.AShape;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.Cube;
import com.opengl.bogdan_liulko.tetrisopengl.utils.CameraController;
import com.opengl.bogdan_liulko.tetrisopengl.utils.Constants;

import java.util.ArrayList;
import java.util.Random;

public class NextFigure extends AShape {

    private static final int POSITION_COUNT = 3;

    private EFigure figure;
    protected ArrayList<Cube> figureCubes;

    public NextFigure(Context context, CameraController cameraController, ArrayList<AShape> shapes) {
        super(context, cameraController);
        figureCubes = new ArrayList<>();
        resetFigure();
        shapes.add(this);
    }

    @Override
    public void draw() {
        if (programId == 0 || aPositionLocation ==  0 || uColorLocation == 0 || uMatrixLocation == 0)
            bindData();
        GLES20.glUseProgram(programId);
        setPosition();
        Matrix.setIdentityM(mModelMatrix, 0);
        bindMatrix();
        GLES20.glLineWidth(Constants.FALLING_CUBE_LINE_WIDTH);
        GLES20.glUniform4f(uColorLocation, Constants.Color.CUBE_LINE_RED, Constants.Color.CUBE_LINE_GREEN, Constants.Color.CUBE_LINE_BLUE, 1);
        GLES20.glDrawArrays(GLES20.GL_LINES, 0, getPointsCount());
        GLES20.glUniform4f(uColorLocation, Constants.Color.FALLING_CUBE_RED, Constants.Color.FALLING_CUBE_GREEN, Constants.Color.FALLING_CUBE_BLUE, 1);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, getPointsCount());
    }

    @Override
    protected int getPositionCount() {
        return POSITION_COUNT;
    }

    @Override
    public void bindMatrix(int width, int height) {
        super.bindMatrix(width, height);
        bindMatrix();
    }

    private void bindMatrix() {
        GLES20.glUseProgram(programId);
        Matrix.multiplyMM(resultMatrix, 0, getCameraMatrix(), 0, mModelMatrix, 0);
        Matrix.multiplyMM(resultMatrix, 0, getProjectionMatrix(), 0, resultMatrix, 0);
        GLES20.glUniformMatrix4fv(uMatrixLocation, 1, false, resultMatrix, 0);
    }

    protected void getFigure() {
        figure = EFigure.getFigureById(getRandomFigureId());
    }

    private int getRandomFigureId() {
        int max = EFigure.values().length - 1;
        int min = 0;
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    public EFigure getNextFigure() {
        return figure;
    }

    public void resetFigure() {
        getFigure();
        addCubes();
    }

    private void addCubes() {
        figureCubes.clear();
        initData(figure.addCubes(figureCubes, getContext(), getCameraController(), Constants.BOXES_PER_COLUMN - 2, -4), POSITION_COUNT);
    }
}
