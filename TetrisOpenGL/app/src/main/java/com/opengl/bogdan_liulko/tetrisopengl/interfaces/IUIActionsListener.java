package com.opengl.bogdan_liulko.tetrisopengl.interfaces;

public interface IUIActionsListener {
    boolean onBackPressed();
    void onPauseClick();
    void onStartClick();
    void onStopClick();
}
