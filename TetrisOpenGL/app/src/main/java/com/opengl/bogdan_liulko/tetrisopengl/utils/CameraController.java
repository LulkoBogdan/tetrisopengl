package com.opengl.bogdan_liulko.tetrisopengl.utils;

import android.opengl.Matrix;
import android.util.Log;

/**
 * Created by bogdan_liulko on 04.03.16.
 */
public class CameraController {

    private final static float MAX_ANGLE = (float) (Math.PI / 2f);

    private final static float upX = 0;
    private final static float upY = 1;
    private final static float upZ = 0;

    private final static float DEFAULT_EYE_X = 0;
    private final static float DEFAULT_EYE_Y = 0;
    private final static float DEFAULT_EYE_Z = 1;
    private final static float DEFAULT_CENTER_X = 0;
    private final static float DEFAULT_CENTER_Y = 0;
    private final static float DEFAULT_CENTER_Z = -1;

    private final float[] mViewMatrix = new float[16];
    private final float[] mProjectionMatrix = new float[16];

    private float eyeX = DEFAULT_EYE_X;
    private float eyeY = DEFAULT_EYE_Y;
    private float eyeZ = DEFAULT_EYE_Z;

    private float centerX = DEFAULT_CENTER_X;
    private float centerY = DEFAULT_CENTER_Y;
    private float centerZ = DEFAULT_CENTER_Z;

    private float currentAngleX;
    private float currentAngleY;

    public CameraController(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ) {
        this.eyeX = eyeX;
        this.eyeY = eyeY;
        this.eyeZ = eyeZ;
        this.centerX = centerX;
        this.centerY = centerY;
        this.centerZ = centerZ;
        updateMatrix();
    }

    public CameraController() {
        updateMatrix();
    }

    public void resetCamera() {
        eyeX = DEFAULT_EYE_X;
        eyeY = DEFAULT_EYE_Y;
        eyeZ = DEFAULT_EYE_Z;
        centerX = DEFAULT_CENTER_X;
        centerY = DEFAULT_CENTER_Y;
        centerZ = DEFAULT_CENTER_Z;
        updateMatrix();
    }

    private void updateMatrix() {
        Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
    }

    public void setProjectionMatrix(int width, int height) {
        float ratio;
        float left = -1;
        float right = 1;
        float bottom = -1;
        float top = 1;
        float far = 3.2f;
        float near = 1.5f;
        if (width > height) {
            ratio = ((float) width) / ((float) height);
            left *= ratio;
            right *= ratio;
        } else {
            ratio = ((float) height) / ((float) width);
            top *= ratio;
            bottom *= ratio;
        }
        Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
    }

    public void setCameraPosition(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ) {
        this.eyeX = eyeX;
        this.eyeY = eyeY;
        this.eyeZ = eyeZ;
        this.centerX = centerX;
        this.centerY = centerY;
        this.centerZ = centerZ;
        updateMatrix();
    }

    public void setEye(float eyeX, float eyeY, float eyeZ) {
        this.eyeX = eyeX;
        this.eyeY = eyeY;
        this.eyeZ = eyeZ;
        updateMatrix();
    }

    public void setCenter(float centerX, float centerY, float centerZ) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.centerZ = centerZ;
        updateMatrix();
    }

    public void moveCamera(float dx, float dy) {
        dx /= 10f;
        dy /= 10f;
        currentAngleX += dx;
        currentAngleY += dy;
        if (currentAngleX > MAX_ANGLE) {
            currentAngleX = MAX_ANGLE;
        } else if (currentAngleX < -MAX_ANGLE) {
            currentAngleX = -MAX_ANGLE;
        }
        if (currentAngleY > MAX_ANGLE) {
            currentAngleY = MAX_ANGLE;
        } else if (currentAngleY < -MAX_ANGLE) {
            currentAngleY = -MAX_ANGLE;
        }
        float eyeX = (float) (Math.cos(currentAngleX) * this.eyeX);
        float eyeY = (float) (Math.cos(currentAngleY) * this.eyeY);
//        float eyeZ = (float) (Math.sin(currentAngleX) * this.eyeZ);

        Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
    }

    public float[] getViewMatrix() {
        return mViewMatrix;
    }

    public float[] getProjectionMatrix() {
        return mProjectionMatrix;
    }

}
