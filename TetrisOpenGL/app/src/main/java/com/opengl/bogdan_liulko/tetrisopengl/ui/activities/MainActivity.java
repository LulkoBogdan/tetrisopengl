package com.opengl.bogdan_liulko.tetrisopengl.ui.activities;

import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opengl.bogdan_liulko.tetrisopengl.R;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.IUIActionsListener;
import com.opengl.bogdan_liulko.tetrisopengl.ui.fragments.FragmentGL;
import com.opengl.bogdan_liulko.tetrisopengl.ui.views.StartDialog;
import com.opengl.bogdan_liulko.tetrisopengl.utils.ScoreController;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private IUIActionsListener uiActionsListener;
    private StartDialog startDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    @Override
    public void onBackPressed() {
        if (uiActionsListener.onBackPressed())
            pause();
        else
            super.onBackPressed();
    }

    @Override
    public void onClick(final View v) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (v.getId()) {
//                    case R.id.rl_dark_background:
                    case R.id.buttonResumeGame:
                        resume();
                        break;
                    case R.id.btn_pause:
                        pause();
                        break;
                    case R.id.buttonStartNewGame:
                        uiActionsListener.onStartClick();
                        startDialog.hideMenu();
                        break;
                    case R.id.buttonBluetoothDuel:
                        //TODO
                        break;
                    case R.id.buttonScoreboard:
                        //TODO go to scoreboard
                        break;
                    case R.id.buttonSettings:
                        //TODO go to settings
                        break;
                    case R.id.buttonExit:
                        uiActionsListener.onStopClick();
                        finish();
                        break;
                }
            }
        }, 200);
    }

    private void initViews() {
        TextView tvScore = (TextView) findViewById(R.id.textViewScore);
        RelativeLayout rlDarkBackground = (RelativeLayout) findViewById(R.id.rl_dark_background);
        RelativeLayout rlMenu = (RelativeLayout) findViewById(R.id.rl_menu_content);
        rlDarkBackground.setOnClickListener(this);
        Button btnResumeGame = (Button) findViewById(R.id.buttonResumeGame);
        btnResumeGame.setOnClickListener(this);
        findViewById(R.id.buttonStartNewGame).setOnClickListener(this);
        findViewById(R.id.buttonBluetoothDuel).setOnClickListener(this);
        findViewById(R.id.buttonScoreboard).setOnClickListener(this);
        findViewById(R.id.buttonSettings).setOnClickListener(this);
        findViewById(R.id.buttonExit).setOnClickListener(this);
        startDialog = (StartDialog) findViewById(R.id.viewMenu);
        startDialog.setViews(rlDarkBackground, rlMenu, btnResumeGame);
        FragmentGL fragmentGL = FragmentGL.getInstance(new ScoreController(tvScore, this), startDialog);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fl_container, fragmentGL).commit();
        uiActionsListener = fragmentGL;
        findViewById(R.id.btn_pause).setOnClickListener(this);
    }

    private void pause() {
        uiActionsListener.onPauseClick();
        startDialog.showMenu(true);
    }

    private void resume() {
        uiActionsListener.onPauseClick();
        startDialog.hideMenu();
    }

}
