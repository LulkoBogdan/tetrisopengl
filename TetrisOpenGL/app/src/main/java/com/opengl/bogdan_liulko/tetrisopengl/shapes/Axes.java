package com.opengl.bogdan_liulko.tetrisopengl.shapes;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.opengl.bogdan_liulko.tetrisopengl.utils.CameraController;
import com.opengl.bogdan_liulko.tetrisopengl.utils.Constants;

import java.util.ArrayList;

public class Axes extends AShape {

    private final static int POSITION_COUNT = 3;

    public Axes(Context context, CameraController cameraController) {
        super(context, cameraController);
        initData(getPoints(), POSITION_COUNT);
        bindData();
    }

    @Override
    public void draw() {
        GLES20.glUseProgram(programId);
        setPosition();
        Matrix.setIdentityM(mModelMatrix, 0);
        bindMatrix();
        GLES20.glUniform4f(uColorLocation, 1, 1, 1, 1);
        GLES20.glDrawArrays(GLES20.GL_LINES, 0, getPointsCount());
    }

    @Override
    protected int getPositionCount() {
        return POSITION_COUNT;
    }

    private float[] getPoints() {
        ArrayList<Float> points = new ArrayList<>();
        float z1 = -1;
        float z2 = z1 + Constants.BOX_DEPTH;
        float startX = Constants.BOX_WIDTH * Constants.BOXES_PER_ROW / 2;
        float x, y;
        addPoint(points, startX, -1, z1);
        addPoint(points, -startX, -1, z1);
        addPoint(points, startX, -1, z2);
        addPoint(points, -startX, -1, z2);

        addPoint(points, -startX, -1, z1);
        addPoint(points, -startX, 1, z1);
        addPoint(points, -startX, -1, z2);
        addPoint(points, -startX, 1, z2);
        for (int i = 0; i <= Constants.BOXES_PER_ROW; i++) {
            x = startX - Constants.BOX_WIDTH * i;
            y = -1;
            addPoint(points, x, y, z1);
            addPoint(points, x, y, z2);
        }
        for (int i = 0; i <= Constants.BOXES_PER_COLUMN; i++) {
            x = -startX;
            y = -1 + Constants.BOX_WIDTH * i;
            addPoint(points, x, y, z1);
            addPoint(points, x, y, z2);
        }
        float[] res = new float[points.size()];
        for (int i = 0; i < points.size(); i++) {
            res[i] = points.get(i);
        }
        return res;
    }

    private void addPoint(ArrayList<Float> list, float x, float y, float z) {
        list.add(x);
        list.add(y);
        list.add(z);
    }

    @Override
    public void bindMatrix(int width, int height) {
        super.bindMatrix(width, height);
        bindMatrix();
    }

    private void bindMatrix() {
        GLES20.glUseProgram(programId);
        Matrix.multiplyMM(resultMatrix, 0, getProjectionMatrix(), 0, getCameraMatrix(), 0);
        Matrix.multiplyMM(resultMatrix, 0, resultMatrix, 0, mModelMatrix, 0);
        GLES20.glUniformMatrix4fv(uMatrixLocation, 1, false, resultMatrix, 0);
    }

}
