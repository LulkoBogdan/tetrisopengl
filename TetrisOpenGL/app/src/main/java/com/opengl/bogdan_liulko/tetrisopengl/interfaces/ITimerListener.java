package com.opengl.bogdan_liulko.tetrisopengl.interfaces;

/**
 * Created by bogdan_liulko on 11.03.16.
 */
public interface ITimerListener {
    void onTime();
}
