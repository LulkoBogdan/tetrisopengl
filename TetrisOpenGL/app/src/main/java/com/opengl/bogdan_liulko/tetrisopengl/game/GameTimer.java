package com.opengl.bogdan_liulko.tetrisopengl.game;

import com.opengl.bogdan_liulko.tetrisopengl.interfaces.ITickListener;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.ITimerListener;

import java.util.ArrayList;
import java.util.Timer;

public class GameTimer implements ITimerListener{

    public final static long ITERATION = 100;
    private final static long DEFAULT_PERIOD = 1500;//1.5 seconds
    private final static long MINIMAL_PERIOD = ITERATION;
    private final static int INCREASE_SPEED_VALUE = 10;

    private Timer timer;
    private GameTimerTask timerTask;
    private ITimerListener timerListener;
    private ArrayList<ITickListener> tickListeners;

    private long period = DEFAULT_PERIOD;
    private int iterationCount;
    private boolean stopped;
    private boolean paused;

    public GameTimer(ITimerListener timerListener) {
        this.timerListener = timerListener;
        tickListeners = new ArrayList<>();
        timer = new Timer();
        timerTask = new GameTimerTask(this);
    }

    public void startCounting() {
        stopped = false;
        paused = false;
        period = DEFAULT_PERIOD;
        iterationCount = 0;
        startTimer();
    }

    public void stopCounting() {
        stopped = true;
        timer.cancel();
        timerTask.cancel();
        timer = null;
        timerTask = null;
    }

    public void pauseCounting() {
        paused = true;
    }

    public void resumeCounting() {
        paused = false;
    }

    public void speedUp() {
        if (period <= MINIMAL_PERIOD)
            period = MINIMAL_PERIOD;
        else
            period -= ITERATION;
    }

    public void resetPeriod() {
        period = DEFAULT_PERIOD;
        iterationCount = 0;
    }

    private void startTimer() {
        timer.scheduleAtFixedRate(timerTask, 0, ITERATION);
    }

    @Override
    public void onTime() {
        if (stopped || paused)
            return;
        iterationCount++;
        if (iterationCount >= period / ITERATION) {
            iterationCount = 0;
            timerListener.onTime();
        }
        notifyTickListeners();
    }

    public void addTickListener(ITickListener tickListener) {
        tickListeners.add(tickListener);
    }

    public void removeTickListener(ITickListener tickListener) {
        tickListeners.remove(tickListener);
    }

    public void clearTickListeners() {
        tickListeners.clear();
    }

    private long speedBeforeSpeedIncrease;

    public void increaseSpeed(boolean increase) {
        if (increase) {
            speedBeforeSpeedIncrease = period;
            if (period - ITERATION * INCREASE_SPEED_VALUE <= MINIMAL_PERIOD)
                period = MINIMAL_PERIOD;
            else
                period -= ITERATION * INCREASE_SPEED_VALUE;
        } else {
            period = speedBeforeSpeedIncrease;
        }
    }

    public void speedDown() {
        period += ITERATION * 3;
    }

    private void notifyTickListeners() {
        ITickListener tickListener;
        for (int i = 0; i < tickListeners.size(); i++) {
            tickListener = tickListeners.get(i);
            tickListener.onTick();
        }
    }
}
