package com.opengl.bogdan_liulko.tetrisopengl.shapes;

/**
 * Created by bogdan_liulko on 01.03.16.
 */
public abstract class BaseShape {

    public abstract void draw();
    public void bindMatrix(int width, int height) {

    }

    public void setMove(float dx, float dy) {

    }

}
