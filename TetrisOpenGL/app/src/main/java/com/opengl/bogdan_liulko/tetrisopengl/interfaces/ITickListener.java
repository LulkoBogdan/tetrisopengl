package com.opengl.bogdan_liulko.tetrisopengl.interfaces;

public interface ITickListener {
    void onTick();
}
