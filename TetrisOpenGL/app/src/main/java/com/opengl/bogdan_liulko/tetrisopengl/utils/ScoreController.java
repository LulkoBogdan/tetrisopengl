package com.opengl.bogdan_liulko.tetrisopengl.utils;

import android.app.Activity;
import android.widget.TextView;

import com.opengl.bogdan_liulko.tetrisopengl.interfaces.IScoreChangeListener;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.Boost;

public class ScoreController implements IScoreChangeListener {

    private final static int LINE_POINTS = 10;

    //Removed one single line +10
    //Every additional line +(10 * n) where n is number of removed line
    //So maximal points value for 4 lines remove is 10 + 20 + 30 + 40 = 100

    //The chain of removes will multiply the points got from previous step with chain number
    //Maximal chain value is 10
    //So maximal points value for n-lines remove and 10 chain remove is 10 * points (10 * 100 = 1000)
    private TextView tvScore;
    private Activity activity;

    private int chain;
    private long score;

    public ScoreController(TextView tvScore, Activity activity) {
        this.tvScore = tvScore;
        this.activity = activity;
        chain = 0;
        score = 0;
        setScore();
    }

    @Override
    public int onLinesRemoved(int countOfLines) {
        if (countOfLines == 0) {
            chain = 0;
            return 0;
        }
        int addScore = getPointsForLines(countOfLines);
        chain++;
        score += addScore * chain;
        setScore();
        return addScore;
    }

    @Override
    public void onGameReset() {
        score = 0;
        chain = 0;
        setScore();
    }

    @Override
    public void onFieldClear() {
        score += LINE_POINTS * 100;
        setScore();
    }

    @Override
    public void onFeatureEnable(Boost boost) {
        if (boost.getFeature().isSupport()) {
            score += LINE_POINTS * 5;
        } else {
            score += LINE_POINTS * 10;
        }
        setScore();
    }

    private void setScore() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvScore.setText(String.valueOf(score));
            }
        });
    }

    private int getPointsForLines(int linesCount) {
        int result = 0;
        for (int i = 1; i <= linesCount; i++) {
            result += i * LINE_POINTS;
        }
        return result;
    }

}
