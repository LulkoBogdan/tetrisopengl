package com.opengl.bogdan_liulko.tetrisopengl.shapes.samples;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.opengl.bogdan_liulko.tetrisopengl.R;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.BaseShape;
import com.opengl.bogdan_liulko.tetrisopengl.utils.ShaderUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by bogdan_liulko on 02.03.16.
 */
public class SampleShapeCamera extends BaseShape {

    private final static int POSITION_COUNT = 3;
    private final static long TIME = 10000;

    private Context context;
    private FloatBuffer vertexData;

    private int uColorLocation;
    private int aPositionLocation;
    private int uMatrixLocation;
    private int programId;
    private float[] mProjectionMatrix = new float[16];
    private float[] mViewMatrix = new float[16];
    private float[] mMatrix = new float[16];

    private float s = 0.4f;
    private float d = 0.9f;
    private float l = 3;
    private float[] vertices = {
            -2 * s, -s, d,
            2 * s, -s, d,
            0, s, d,

            -2 * s, -s, -d,
            2 * s, -s, -d,
            0, s, -d,

            d, -s, -2 * s,
            d, -s, 2 * s,
            d, s, 0,

            -d, -s, -2 * s,
            -d, -s, 2 * s,
            -d, s, 0,

            -l, 0, 0,
            l, 0, 0,

            0, -l, 0,
            0, l, 0,

            0, 0, -l,
            0, 0, l
    };

    public SampleShapeCamera(Context context) {
        this.context = context;
        vertexData = ByteBuffer
                .allocateDirect(vertices.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        vertexData.put(vertices);

        int vertexShaderId = ShaderUtils.createShader(context, GLES20.GL_VERTEX_SHADER, R.raw.vertex_shader_3d);
        int fragmentShaderId = ShaderUtils.createShader(context, GLES20.GL_FRAGMENT_SHADER, R.raw.fragment_shader);
        programId = ShaderUtils.createProgram(vertexShaderId, fragmentShaderId);
        GLES20.glUseProgram(programId);

        createViewMatrix();

        prepareData();
    }

    @Override
    public void draw() {
        createViewMatrix();
        bindMatrix();
        GLES20.glUniform4f(uColorLocation, 0.0f, 1.0f, 0.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 3);

        GLES20.glUniform4f(uColorLocation, 0.0f, 0.0f, 1.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 3, 3);

        GLES20.glUniform4f(uColorLocation, 1.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 6, 3);

        GLES20.glUniform4f(uColorLocation, 1.0f, 1.0f, 0.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 9, 3);

        GLES20.glLineWidth(5);

        GLES20.glUniform4f(uColorLocation, 0.0f, 1.0f, 1.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_LINES, 12, 2);

        GLES20.glUniform4f(uColorLocation, 1.0f, 0.0f, 1.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_LINES, 14, 2);

        GLES20.glUniform4f(uColorLocation, 1.0f, 0.5f, 0.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_LINES, 16, 2);
    }

    @Override
    public void bindMatrix(int width, int height) {
        float ratio;
        float left = -1.0f;
        float right = 1.0f;
        float top = 1.0f;
        float bottom = -1.0f;
        float near = 2.0f;
        float far = 8.0f;
        if (width > height) {
            ratio = ((float) width) / ((float) height);
            left *= ratio;
            right *= ratio;
        } else {
            ratio = ((float) height) / ((float) width);
            bottom *= ratio;
            top *= ratio;
        }

        Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
        bindMatrix();
    }

    private void bindMatrix() {
        Matrix.multiplyMM(mMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);
        GLES20.glUniformMatrix4fv(uMatrixLocation, 1, false, mMatrix, 0);
    }

    private void createViewMatrix() {
        float time = (float) (System.currentTimeMillis() % TIME) / TIME;
        float angle = time * 2 * 3.1415926f;
        float eyeX = (float) (Math.cos(angle) * 4f);//0;
        float eyeY = 1f;//0;
        float eyeZ = 4f;//3;

        float centerX = 0;
        float centerY = 0;
        float centerZ = 0;

        float upX = 0;
        float upY = 1;
        float upZ = 0;

        Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
    }

    private void prepareData() {
        aPositionLocation = GLES20.glGetAttribLocation(programId, "a_Position");
        vertexData.position(0);
        GLES20.glVertexAttribPointer(aPositionLocation, POSITION_COUNT, GLES20.GL_FLOAT, false, 0, vertexData);
        GLES20.glEnableVertexAttribArray(aPositionLocation);

        uColorLocation = GLES20.glGetUniformLocation(programId, "u_Color");

        uMatrixLocation = GLES20.glGetUniformLocation(programId, "u_Matrix");
    }
}
