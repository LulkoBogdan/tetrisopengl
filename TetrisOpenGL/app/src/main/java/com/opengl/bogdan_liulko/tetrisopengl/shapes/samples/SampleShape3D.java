package com.opengl.bogdan_liulko.tetrisopengl.shapes.samples;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.opengl.bogdan_liulko.tetrisopengl.R;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.BaseShape;
import com.opengl.bogdan_liulko.tetrisopengl.utils.ShaderUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by bogdan_liulko on 02.03.16.
 */
public class SampleShape3D extends BaseShape {

    private final static int POSITION_COUNT = 3;//4;

    private Context context;
    private FloatBuffer vertexData;

    private int uColorLocation;
    private int aPositionLocation;
    private int uMatrixLocation;
    private float[] mProjectionMatrix = new float[16];

    private float z1 = -1.0f, z2 = -1.0f;
    private float[] vertices = {
            -0.7f, -0.5f, z1,
            0.3f, -0.5f, z1,
            -0.2f, 0.3f, z1,

            -0.3f, -0.4f, z2,
            0.7f, -0.4f, z2,
            0.2f, 0.4f, z2
    };

    public SampleShape3D(Context context) {
        this.context = context;
        vertexData = ByteBuffer
                .allocateDirect(vertices.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        vertexData.put(vertices);

        int vertexShaderId = ShaderUtils.createShader(context, GLES20.GL_VERTEX_SHADER, R.raw.vertex_shader_3d);
        int fragmentShaderId = ShaderUtils.createShader(context, GLES20.GL_FRAGMENT_SHADER, R.raw.fragment_shader);
        int programId = ShaderUtils.createProgram(vertexShaderId, fragmentShaderId);
        GLES20.glUseProgram(programId);

        aPositionLocation = GLES20.glGetAttribLocation(programId, "a_Position");
        vertexData.position(0);
        GLES20.glVertexAttribPointer(aPositionLocation, POSITION_COUNT, GLES20.GL_FLOAT, false, 0, vertexData);
        GLES20.glEnableVertexAttribArray(aPositionLocation);

        uColorLocation = GLES20.glGetUniformLocation(programId, "u_Color");

        uMatrixLocation = GLES20.glGetUniformLocation(programId, "u_Matrix");
    }

    @Override
    public void draw() {
        GLES20.glUniform4f(uColorLocation, 0.0f, 1.0f, 0.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 3);

        GLES20.glUniform4f(uColorLocation, 0.0f, 0.0f, 1.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 3, 3);
    }

    @Override
    public void bindMatrix(int width, int height) {
        float ratio;
        float left = -1.0f;
        float right = 1.0f;
        float top = 1.0f;
        float bottom = -1.0f;
        float near = 1.0f;
        float far = 8.0f;
        if (width > height) {
            ratio = ((float) width) / ((float) height);
            left *= ratio;
            right *= ratio;
        } else {
            ratio = ((float) height) / ((float) width);
            bottom *= ratio;
            top *= ratio;
        }

        Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
        GLES20.glUniformMatrix4fv(uMatrixLocation, 1, false, mProjectionMatrix, 0);
    }
}
