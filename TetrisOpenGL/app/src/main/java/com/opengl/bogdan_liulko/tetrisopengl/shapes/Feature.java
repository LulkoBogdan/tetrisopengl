package com.opengl.bogdan_liulko.tetrisopengl.shapes;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.opengl.bogdan_liulko.tetrisopengl.R;
import com.opengl.bogdan_liulko.tetrisopengl.enums.EFeature;
import com.opengl.bogdan_liulko.tetrisopengl.game.GameTimer;
import com.opengl.bogdan_liulko.tetrisopengl.utils.CameraController;
import com.opengl.bogdan_liulko.tetrisopengl.utils.GridController;
import com.opengl.bogdan_liulko.tetrisopengl.utils.ShaderUtils;
import com.opengl.bogdan_liulko.tetrisopengl.utils.TextureUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class Feature extends AShape{

    private final static int POINTS_COUNT = 3;
    private static final int TEXTURE_COUNT = 2;

    private int row;
    private int column;
    private int timeLeft;
    private int texture;
    private int aTextureLocation;
    private int uTextureUnitLocation;

    private EFeature feature;
    private FloatBuffer vertexDataTexture;

    public Feature(Context context, CameraController cameraController, EFeature feature, int row, int column, int timeLeft) {
        super(context, cameraController);
        this.feature = feature;
        this.column = column;
        this.timeLeft = timeLeft;
        moveToRow(row);
    }

    public int getRow() {
        return row;
    }

    public int onTimeTick() {
        timeLeft -= GameTimer.ITERATION;
        return timeLeft;
    }

    public EFeature getFeature() {
        return feature;
    }

    public void moveToRow(int row) {
        this.row = row;
        initData(getPoints(), getPositionCount());
        initDataTexture(getPointsTexture());
    }

    @Override
    public void draw() {
        if (programId == 0 || texture == 0) {
            texture = TextureUtils.loadTexture(getContext(), feature.getTextureId());
            bindData();
        }
        GLES20.glUseProgram(programId);
        vertexData.position(0);
        vertexDataTexture.position(0);
        setPosition();
        bindMatrix();
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, getPointsCount());
    }

    @Override
    protected int getPositionCount() {
        return POINTS_COUNT;
    }

    @Override
    public void bindMatrix(int width, int height) {
        super.bindMatrix(width, height);
        bindMatrix();
    }

    @Override
    protected void bindData() {
        int vertexShaderId = ShaderUtils.createShader(getContext(), GLES20.GL_VERTEX_SHADER, R.raw.vertex_shader_feature);
        int fragmentShaderId = ShaderUtils.createShader(getContext(), GLES20.GL_FRAGMENT_SHADER, R.raw.fragment_shader_feature);
        programId = ShaderUtils.createProgram(vertexShaderId, fragmentShaderId);
        GLES20.glUseProgram(programId);
        aPositionLocation = GLES20.glGetAttribLocation(programId, "a_Position");
        uMatrixLocation = GLES20.glGetUniformLocation(programId, "u_Matrix");
        aTextureLocation = GLES20.glGetAttribLocation(programId, "a_Texture");
        uTextureUnitLocation = GLES20.glGetUniformLocation(programId, "u_TextureUnit");

        addTexture();
    }

    @Override
    protected void setPosition() {
        GLES20.glVertexAttribPointer(aPositionLocation, POINTS_COUNT, GLES20.GL_FLOAT, false, 0, vertexData);
        GLES20.glEnableVertexAttribArray(aPositionLocation);
        GLES20.glVertexAttribPointer(aTextureLocation, TEXTURE_COUNT, GLES20.GL_FLOAT, false, 0, vertexDataTexture);
        GLES20.glEnableVertexAttribArray(aTextureLocation);
    }

    private void addTexture() {
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture);
        GLES20.glUniform1i(uTextureUnitLocation, 0);
    }

    private void bindMatrix() {
        GLES20.glUseProgram(programId);
        Matrix.multiplyMM(resultMatrix, 0, getProjectionMatrix(), 0, getCameraMatrix(), 0);
        GLES20.glUniformMatrix4fv(uMatrixLocation, 1, false, resultMatrix, 0);
    }

    private float[] getPoints() {
        return GridController.getFeatureCubeCoordinates(row, column, this instanceof Boost);
    }

    private void initDataTexture(float[] vertices) {
        vertexDataTexture = ByteBuffer
                .allocateDirect(vertices.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        vertexDataTexture.put(vertices);
    }

    private float[] getPointsTexture() {
        float[] res = new float[getPointsCount()];
        int added = 0;
        added = addTexturePoint(0, 1, added, res);
        added = addTexturePoint(1, 1, added, res);
        added = addTexturePoint(1, 0, added, res);
        added = addTexturePoint(0, 1, added, res);
        added = addTexturePoint(1, 0, added, res);
        addTexturePoint(0, 0, added, res);
        return res;
    }

    private int addTexturePoint(float x, float y, int added, float[] res) {
        res[added] = x;
        res[added + 1] = y;
        return added + 2;
    }

}
