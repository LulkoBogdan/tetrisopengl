package com.opengl.bogdan_liulko.tetrisopengl.enums;

import android.content.Context;

import com.opengl.bogdan_liulko.tetrisopengl.shapes.Cube;
import com.opengl.bogdan_liulko.tetrisopengl.utils.CameraController;
import com.opengl.bogdan_liulko.tetrisopengl.utils.Constants;

import java.util.ArrayList;

public enum EFigure {
    G_RIGHT(new int[][]{{-2, 0}, {-1, 0}, {-3, 0} ,{-1, 1}}),
    G_LEFT(new int[][]{{-2, 0}, {-1, 0}, {-3, 0} ,{-1, -1}}),
    SQUARE(new int[][]{{-1, 0}, {-2, 0}, {-2, 1} ,{-1, 1}}),
    TRIANGLE(new int[][]{{-2, 0}, {-1, 0}, {-2, -1} ,{-2, 1}}),
    ZIGZAG_LEFT(new int[][]{{-2, 1}, {-2, 2}, {-1, 0} ,{-1, 1}}),
    ZIGZAG_RIGHT(new int[][]{{-2, 0}, {-1, 0}, {-2, -1} ,{-1, 1}}),
    STICK(new int[][]{{-3, 0}, {-2, 0}, {-1, 0} ,{-4, 0}});

    private final int [][] cubesCoordinates;

    EFigure(int[][] cubesCoordinates) {
        this.cubesCoordinates = cubesCoordinates;
    }

    public static EFigure getFigureById(int id) {
        EFigure[] figures = values();
        if (id < 0 || id >= figures.length)
            return null;
        return figures[id];
    }

    public float[] addCubes(ArrayList<Cube> cubes, Context context, CameraController cameraController, int startRow, int startColumn) {
        float[] vertices = new float[6 * 6 * 3 * 4];
        Cube cube;
        int cubeNumber = 0;
        int baseIndex;
        int[] cubesCoordinate;
        for (int i = 0; i < cubesCoordinates.length; i++) {
            cubesCoordinate = cubesCoordinates[i];
            cube = new Cube(context, cameraController, startRow + cubesCoordinate[0], startColumn + cubesCoordinate[1]);
            cube.setMain(i == 0);
            cubes.add(cube);
            float[] cubeVertices = cube.getVertices();
            baseIndex = cubeNumber * cubeVertices.length;
            System.arraycopy(cubeVertices, 0, vertices, baseIndex, cubeVertices.length);
            cubeNumber++;
        }
        return vertices;
    }

    public float[] addCubes(ArrayList<Cube> cubes, Context context, CameraController cameraController) {
        return addCubes(cubes, context, cameraController, Constants.BOXES_PER_COLUMN, Constants.BOXES_PER_ROW / 2);
    }
}
