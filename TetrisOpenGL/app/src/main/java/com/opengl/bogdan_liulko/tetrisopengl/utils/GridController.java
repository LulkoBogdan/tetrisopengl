package com.opengl.bogdan_liulko.tetrisopengl.utils;

import com.opengl.bogdan_liulko.tetrisopengl.shapes.Cube;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

public class GridController {

    private final static float START_LEVEL = -1;
    private final static float START_LEVEL_X = -(Constants.BOXES_PER_ROW / 2f * Constants.BOX_WIDTH);

    public static float[] getCubeCoordinates(int row, int column) {
        float z1 = START_LEVEL;
        float z2 = z1 + Constants.BOX_DEPTH;
        float x1 = START_LEVEL_X + column * Constants.BOX_WIDTH;
        float x2 = x1 + Constants.BOX_WIDTH;
        float y1 = START_LEVEL + row * Constants.BOX_WIDTH;
        float y2 = y1 + Constants.BOX_WIDTH;
        float[] points = new float[6 * 6 * 3];
        int added = 0;

        //back side
        added = addPoint(points, x1, y1, z1, added);
        added = addPoint(points, x2, y1, z1, added);
        added = addPoint(points, x2, y2, z1, added);

        added = addPoint(points, x1, y1, z1, added);
        added = addPoint(points, x2, y2, z1, added);
        added = addPoint(points, x1, y2, z1, added);

        //front side
        added = addPoint(points, x1, y1, z2, added);
        added = addPoint(points, x2, y1, z2, added);
        added = addPoint(points, x2, y2, z2, added);

        added = addPoint(points, x1, y1, z2, added);
        added = addPoint(points, x2, y2, z2, added);
        added = addPoint(points, x1, y2, z2, added);

        //left side
        added = addPoint(points, x1, y1, z1, added);
        added = addPoint(points, x1, y2, z1, added);
        added = addPoint(points, x1, y1, z2, added);

        added = addPoint(points, x1, y2, z1, added);
        added = addPoint(points, x1, y1, z2, added);
        added = addPoint(points, x1, y2, z2, added);

        //right side
        added = addPoint(points, x2, y1, z1, added);
        added = addPoint(points, x2, y2, z1, added);
        added = addPoint(points, x2, y1, z2, added);

        added = addPoint(points, x2, y2, z1, added);
        added = addPoint(points, x2, y1, z2, added);
        added = addPoint(points, x2, y2, z2, added);

        //top
        added = addPoint(points, x1, y2, z1, added);
        added = addPoint(points, x2, y2, z1, added);
        added = addPoint(points, x2, y2, z2, added);

        added = addPoint(points, x1, y2, z1, added);
        added = addPoint(points, x1, y2, z2, added);
        added = addPoint(points, x2, y2, z2, added);

        //bottom
        added = addPoint(points, x1, y1, z1, added);
        added = addPoint(points, x2, y1, z1, added);
        added = addPoint(points, x2, y1, z2, added);

        added = addPoint(points, x1, y1, z1, added);
        added = addPoint(points, x1, y1, z2, added);
        addPoint(points, x2, y1, z2, added);

        return points;
    }

    public static float[] getShadowCoordinates(HashMap<Integer, Cube> shadowPositions, Cube[][] cubeGrid) {
        float z1 = START_LEVEL;
        float z2 = z1 + Constants.BOX_DEPTH;
        float x1;
        float x2;
        float y1;
        float[] points = new float[6 * 6 * 3 * shadowPositions.size()];
        int added = 0;
        HashMap<Integer, Integer> nearestSurface = new HashMap<>();
        Set<Integer> keys = shadowPositions.keySet();
        for (Integer position : keys) {
            nearestSurface.put(position, 0);
            for (int i = shadowPositions.get(position).getCurrentRow(); i >= 0; i--) {
                if (cubeGrid[position][i] != null) {
                    nearestSurface.put(position, i + 1);
                    break;
                }
            }
        }
        for (Integer position : keys) {
            x1 = START_LEVEL_X + position * Constants.BOX_WIDTH;
            x2 = x1 + Constants.BOX_WIDTH;
            y1 = START_LEVEL + nearestSurface.get(position) * Constants.BOX_WIDTH + 0.005f;
            added = addPoint(points, x1, y1, z1, added);
            added = addPoint(points, x2, y1, z1, added);
            added = addPoint(points, x2, y1, z2, added);

            added = addPoint(points, x1, y1, z1, added);
            added = addPoint(points, x1, y1, z2, added);
            added = addPoint(points, x2, y1, z2, added);
        }
        return points;
    }

    private static int addPoint(float[] array, float x, float y, float z, int position) {
        array[position] = x;
        array[position + 1] = y;
        array[position + 2] = z;
        return position + 3;
    }

    public static boolean checkAxesBounds(int row, int column) {
        return row >= 0 && column >= 0 && row < Constants.BOXES_PER_COLUMN && column < Constants.BOXES_PER_ROW;
    }

    public static boolean getCubeGrid(ArrayList<Cube> cubes, Cube[][] cubeGrid) {
        for (Cube[] aCubeGrid : cubeGrid) {
            Arrays.fill(aCubeGrid, null);
        }
        for (Cube cube : cubes) {
            if (cube.getCurrentRow() == Constants.BOXES_PER_COLUMN - 1) {
                return true;
            }
            cubeGrid[cube.getCurrentColumn()][cube.getCurrentRow()] = cube;
        }
        return false;
    }

    public static boolean getCubeGrid(ArrayList<Cube> cubes, Cube[][] cubeGrid, HashMap<Integer, Integer> filledRows) {
        for (int i = 0; i < Constants.BOXES_PER_COLUMN; i++) {
            filledRows.put(i, 0);
        }
        for (Cube[] aCubeGrid : cubeGrid) {
            Arrays.fill(aCubeGrid, null);
        }
        for (Cube cube : cubes) {
            if (cube.getCurrentRow() == Constants.BOXES_PER_COLUMN - 1) {
                return true;
            }
            cubeGrid[cube.getCurrentColumn()][cube.getCurrentRow()] = cube;
            filledRows.put(cube.getCurrentRow(), filledRows.get(cube.getCurrentRow()) + 1);
        }
        return false;
    }

    public static float[] getFeatureCubeCoordinates(int row, int column, boolean isBoost) {
        float boxWidth = isBoost ? 1.5f * Constants.BOX_WIDTH : Constants.BOX_WIDTH;
        float z1 = START_LEVEL;
        float z2 = z1 + Constants.BOX_DEPTH * 1.01f;
        float x1 = START_LEVEL_X + column * Constants.BOX_WIDTH;
        float x2 = x1 + boxWidth;
        float y1 = START_LEVEL + row * Constants.BOX_WIDTH;
        float y2 = y1 + boxWidth;
        float[] points = new float[6 * 6 * 3];
        int added = 0;

        //front side
        added = addPoint(points, x1, y1, z2, added);
        added = addPoint(points, x2, y1, z2, added);
        added = addPoint(points, x2, y2, z2, added);

        added = addPoint(points, x1, y1, z2, added);
        added = addPoint(points, x2, y2, z2, added);
        addPoint(points, x1, y2, z2, added);

        return points;
    }

}
