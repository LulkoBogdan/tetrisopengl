package com.opengl.bogdan_liulko.tetrisopengl.ui.fragments;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.opengl.bogdan_liulko.tetrisopengl.gl.MyGLSurfaceView;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.IUIActionsListener;
import com.opengl.bogdan_liulko.tetrisopengl.ui.views.StartDialog;
import com.opengl.bogdan_liulko.tetrisopengl.utils.ScoreController;

public class FragmentGL extends Fragment implements IUIActionsListener{

    public static FragmentGL getInstance(ScoreController scoreController, StartDialog startDialog) {
        FragmentGL fragmentGL = new FragmentGL();
        fragmentGL.setScoreController(scoreController);
        fragmentGL.setStartDialog(startDialog);
        return fragmentGL;
    }

    private MyGLSurfaceView glSurfaceView;
    private ScoreController scoreController;
    private StartDialog startDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (!supportES2()) {
            Toast.makeText(getActivity(), "OpenGl ES 2.0 is not supported", Toast.LENGTH_LONG).show();
            getActivity().finish();
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        glSurfaceView = new MyGLSurfaceView(getActivity());
        glSurfaceView.setScoreController(scoreController);
        glSurfaceView.setStartDialog(startDialog);
        return glSurfaceView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (glSurfaceView != null) {
            glSurfaceView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (glSurfaceView != null) {
            glSurfaceView.onPause();
        }
    }

    private boolean supportES2() {
        ActivityManager activityManager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();
        return configurationInfo.reqGlEsVersion >= 0x20000;
    }

    @Override
    public boolean onBackPressed() {
        return glSurfaceView.onBackPressed();
    }

    @Override
    public void onPauseClick() {
        glSurfaceView.onPauseClick();
    }

    @Override
    public void onStartClick() {
        glSurfaceView.onStartClick();
    }

    @Override
    public void onStopClick() {
        glSurfaceView.onStopClick();
    }

    public void setScoreController(ScoreController scoreController) {
        this.scoreController = scoreController;
    }

    public void setStartDialog(StartDialog startDialog) {
        this.startDialog = startDialog;
    }
}
