package com.opengl.bogdan_liulko.tetrisopengl.shapes;

import android.content.Context;
import android.opengl.GLES20;

import com.opengl.bogdan_liulko.tetrisopengl.R;
import com.opengl.bogdan_liulko.tetrisopengl.utils.CameraController;
import com.opengl.bogdan_liulko.tetrisopengl.utils.ShaderUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public abstract class AShape {

    private Context context;
    private CameraController cameraController;
    private int pointSize;

    protected int uColorLocation;
    protected int uMatrixLocation;
    protected int aPositionLocation;
    protected int programId;
    protected float[] vertices;
    protected float[] resultMatrix = new float[16];
    protected float[] mModelMatrix = new float[16];
    protected FloatBuffer vertexData;

    public AShape(Context context, CameraController cameraController) {
        this.context = context;
        this.cameraController = cameraController;
    }

    public abstract void draw();
    protected abstract int getPositionCount();

    public void bindMatrix(int width, int height) {
        cameraController.setProjectionMatrix(width, height);
    }

    public Context getContext() {
        return context;
    }

    public CameraController getCameraController() {
        return cameraController;
    }

    protected void initData(float[] vertices, int pointSize) {
        this.vertices = vertices;
        this.pointSize = pointSize;
        vertexData = ByteBuffer
                .allocateDirect(vertices.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        vertexData.put(vertices);
    }

    protected int getPointsCount() {
        return vertices.length / pointSize;
    }

    protected float[] getCameraMatrix() {
        return cameraController.getViewMatrix();
    }

    protected float[] getProjectionMatrix() {
        return cameraController.getProjectionMatrix();
    }

    protected void bindData() {
        int vertexShaderId = ShaderUtils.createShader(getContext(), GLES20.GL_VERTEX_SHADER, R.raw.vertex_shader_axes);
        int fragmentShaderId = ShaderUtils.createShader(getContext(), GLES20.GL_FRAGMENT_SHADER, R.raw.fragment_shader_axes);
        programId = ShaderUtils.createProgram(vertexShaderId, fragmentShaderId);
        GLES20.glUseProgram(programId);
        aPositionLocation = GLES20.glGetAttribLocation(programId, "a_Position");
        vertexData.position(0);
        setPosition();
        uColorLocation = GLES20.glGetUniformLocation(programId, "u_Color");
        uMatrixLocation = GLES20.glGetUniformLocation(programId, "u_Matrix");
    }

    protected void setPosition() {
        GLES20.glVertexAttribPointer(aPositionLocation, getPositionCount(), GLES20.GL_FLOAT, false, 0, vertexData);
        GLES20.glEnableVertexAttribArray(aPositionLocation);
    }

}
