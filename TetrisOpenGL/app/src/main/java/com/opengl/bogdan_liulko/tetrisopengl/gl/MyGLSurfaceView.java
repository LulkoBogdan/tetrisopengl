package com.opengl.bogdan_liulko.tetrisopengl.gl;

import android.app.Activity;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

import com.opengl.bogdan_liulko.tetrisopengl.enums.EAction;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.IRedrawCallback;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.IUIActionsListener;
import com.opengl.bogdan_liulko.tetrisopengl.ui.views.StartDialog;
import com.opengl.bogdan_liulko.tetrisopengl.utils.ScoreController;

public class MyGLSurfaceView extends GLSurfaceView implements IUIActionsListener{

    private final static float ACTION_DELTA = 50;

    private final GLRenderer renderer;
    private final RedrawCallback redrawCallback;

    public MyGLSurfaceView(Context context) {
        super(context);
        setEGLContextClientVersion(2);
        redrawCallback = new RedrawCallback(context, this);
        renderer = new GLRenderer(context, redrawCallback);
        setRenderer(renderer);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);//GLSurfaceView.RENDERMODE_CONTINUOUSLY);
//        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }

    private boolean actionStarted;
    private boolean dropAllowed;
    private boolean rotateAllowed;
    private float startX;
    private float startY;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                if (actionStarted) {
                    float dX = startX - x;
                    float dY = startY - y;
                    EAction action = getAction(dX, dY);
                    if (action == null || action == EAction.DROP && !dropAllowed)
                        break;
                    if (action == EAction.DROP)
                        actionStarted = false;
                    startX = x;
                    startY = y;
                    rotateAllowed = false;
                    renderer.setAction(action);
                    redrawCallback.redraw();
                }
                break;
            case MotionEvent.ACTION_DOWN:
                actionStarted = true;
                dropAllowed = true;
                rotateAllowed = true;
                startX = x;
                startY = y;
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                if (actionStarted && rotateAllowed) {
                    actionStarted = false;
                    renderer.setAction(EAction.ROTATE);
                    redrawCallback.redraw();
                }
                break;
        }
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
        renderer.stopGame();
    }

    private EAction getAction(float dx, float dy) {
        if (Math.abs(dx) > ACTION_DELTA) {
            return dx < 0 ? EAction.MOVE_RIGHT : EAction.MOVE_LEFT;
        }
        if (Math.abs(dy) > ACTION_DELTA) {
            return dy > 0 ? EAction.UP : EAction.DROP;
        }
        return null;
    }

    @Override
    public boolean onBackPressed() {
        return renderer.onBackPressed();
    }

    @Override
    public void onPauseClick() {
        renderer.onPauseClick();
    }

    @Override
    public void onStartClick() {
        renderer.onStartClick();
    }

    @Override
    public void onStopClick() {
        renderer.onStopClick();
    }

    private static class RedrawCallback implements IRedrawCallback{

        private final Activity activity;
        private final GLSurfaceView surfaceView;

        public RedrawCallback(Context context, GLSurfaceView surfaceView) {
            this.activity = (Activity) context;
            this.surfaceView = surfaceView;
        }

        @Override
        public void redraw() {
            if (activity.isFinishing())
                return;
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    surfaceView.requestRender();
                }
            });
        }
    }

    public void setScoreController(ScoreController scoreController) {
        renderer.setScoreController(scoreController);
    }

    public void setStartDialog(StartDialog startDialog) {
        renderer.setStartDialog(startDialog);
    }
}
