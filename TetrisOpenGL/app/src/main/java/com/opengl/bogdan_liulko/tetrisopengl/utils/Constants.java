package com.opengl.bogdan_liulko.tetrisopengl.utils;

public class Constants {
    public static final int BOXES_PER_ROW = 11;
    public static final int BOXES_PER_COLUMN = 17;
    public static final float BOX_WIDTH = Math.min(2f /((float) BOXES_PER_COLUMN), 2f / ((float) BOXES_PER_ROW));
    public static final float BOX_DEPTH = BOX_WIDTH /*/ 2f*/;

    public static final float PLACED_CUBE_LINE_WIDTH = 2f;
    public static final float FALLING_CUBE_LINE_WIDTH = 5f;

    public static class Color{
        public static final float PLACED_CUBE_RED = 0;
        public static final float PLACED_CUBE_GREEN = 0;
        public static final float PLACED_CUBE_BLUE = 0.5f;

        public static final float FALLING_CUBE_RED = 0;
        public static final float FALLING_CUBE_GREEN = 0;
        public static final float FALLING_CUBE_BLUE = 0.7f;

        public static final float CUBE_LINE_RED = 1;
        public static final float CUBE_LINE_GREEN = 1;
        public static final float CUBE_LINE_BLUE = 0;
    }
}
