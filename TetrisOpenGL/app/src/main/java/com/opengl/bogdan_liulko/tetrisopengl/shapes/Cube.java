package com.opengl.bogdan_liulko.tetrisopengl.shapes;

import android.content.Context;

import com.opengl.bogdan_liulko.tetrisopengl.utils.CameraController;
import com.opengl.bogdan_liulko.tetrisopengl.utils.GridController;

public class Cube extends AShape {

    private final static int POSITION_COUNT = 3;

    private int currentRow;
    private int currentColumn;
    private boolean main;

    public Cube(Context context, CameraController cameraController, int row, int column) {
        super(context, cameraController);
        updateCube(row, column);
    }

    @Override
    public void draw() {
    }

    @Override
    protected int getPositionCount() {
        return POSITION_COUNT;
    }

    private float[] getPoints() {
        return GridController.getCubeCoordinates(currentRow, currentColumn);
    }

    public void moveTo(int row, int column) {
        currentColumn = column;
        currentRow = row;
    }

    public int getCurrentRow() {
        return currentRow;
    }

    public int getCurrentColumn() {
        return currentColumn;
    }

    public float[] getVertices() {
        return vertices;
    }

    public void updateCube(int row, int column) {
        currentRow = row;
        currentColumn = column;
        initData(getPoints(), POSITION_COUNT);
        main = false;
    }

    public void rotateCube(int row, int column, int deltaY, int deltaX) {
        currentRow = row;
        currentColumn = column;
        initData(GridController.getCubeCoordinates(row - deltaY, column - deltaX), POSITION_COUNT);
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }
}
