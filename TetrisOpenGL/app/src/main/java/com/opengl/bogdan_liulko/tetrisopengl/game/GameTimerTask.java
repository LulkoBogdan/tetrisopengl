package com.opengl.bogdan_liulko.tetrisopengl.game;

import com.opengl.bogdan_liulko.tetrisopengl.interfaces.ITimerListener;

import java.util.TimerTask;

/**
 * Created by bogdan_liulko on 11.03.16.
 */
public class GameTimerTask extends TimerTask {

    private ITimerListener timerListener;

    public GameTimerTask(ITimerListener timerListener) {
        this.timerListener = timerListener;
    }

    @Override
    public void run() {
        timerListener.onTime();
    }
}
