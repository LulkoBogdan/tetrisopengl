package com.opengl.bogdan_liulko.tetrisopengl.shapes.figures;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.opengl.bogdan_liulko.tetrisopengl.shapes.AShape;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.Cube;
import com.opengl.bogdan_liulko.tetrisopengl.utils.CameraController;
import com.opengl.bogdan_liulko.tetrisopengl.utils.Constants;
import com.opengl.bogdan_liulko.tetrisopengl.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;

public class PlacedFigure extends AShape{

    private final static int POSITION_COUNT = 3;

    private ArrayList<Cube> cubes;
    private float[] points;

    public PlacedFigure(Context context, CameraController cameraController, ArrayList<AShape> shapes) {
        super(context, cameraController);
        this.cubes = new ArrayList<>();
        shapes.add(this);
        points = new float[6 * 6 * 3 * Constants.BOXES_PER_COLUMN * Constants.BOXES_PER_ROW];
    }

    public void addCubes(ArrayList<Cube> figureCubes) {
        Cube cube;
        float[] vertices;
        int index = cubes.size();
        int baseIndex;
        for (int i = 0; i < figureCubes.size(); i++) {
            cube = figureCubes.get(i);
            cube.updateCube(cube.getCurrentRow(), cube.getCurrentColumn());
            vertices = cube.getVertices();
            cubes.add(cube);
            baseIndex = index * vertices.length;
            System.arraycopy(vertices, 0, points, baseIndex, vertices.length);
            index++;
        }
        initData(points, POSITION_COUNT);
    }

    public ArrayList<Cube> getPlacedCubes() {
        return cubes;
    }

    @Override
    public void draw() {
        if (programId == 0 || aPositionLocation ==  0 || uColorLocation == 0 || uMatrixLocation == 0)
            bindData();
        GLES20.glUseProgram(programId);
        setPosition();
        Matrix.setIdentityM(mModelMatrix, 0);
        bindMatrix();
        GLES20.glLineWidth(Constants.PLACED_CUBE_LINE_WIDTH);
        GLES20.glUniform4f(uColorLocation, Constants.Color.CUBE_LINE_RED, Constants.Color.CUBE_LINE_GREEN, Constants.Color.CUBE_LINE_BLUE, 1);
        GLES20.glDrawArrays(GLES20.GL_LINES, 0, getPointsCount());
        GLES20.glUniform4f(uColorLocation, Constants.Color.PLACED_CUBE_RED, Constants.Color.PLACED_CUBE_GREEN, Constants.Color.PLACED_CUBE_BLUE, 1);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, getPointsCount());
    }

    @Override
    protected int getPositionCount() {
        return POSITION_COUNT;
    }

    @Override
    public void bindMatrix(int width, int height) {
        super.bindMatrix(width, height);
        bindMatrix();
    }

    private void bindMatrix() {
        GLES20.glUseProgram(programId);
        Matrix.multiplyMM(resultMatrix, 0, getCameraMatrix(), 0, mModelMatrix, 0);
        Matrix.multiplyMM(resultMatrix, 0, getProjectionMatrix(), 0, resultMatrix, 0);
        GLES20.glUniformMatrix4fv(uMatrixLocation, 1, false, resultMatrix, 0);
    }

    public void clearCubes() {
        cubes.clear();
        Arrays.fill(points, 0);
    }

    public void removeRows(ArrayList<Integer> rowsToRemove) {
        Cube cube;
        ArrayList<Cube> cubesToRemove = new ArrayList<>();
        int numberToShiftDown;
        for (int i = 0; i < cubes.size(); i++) {
            cube = cubes.get(i);
            if (rowsToRemove.contains(cube.getCurrentRow())) {
                cubesToRemove.add(cube);
            } else if ((numberToShiftDown = getNumberToShiftDown(rowsToRemove, cube)) > 0) {
                cube.updateCube(cube.getCurrentRow() - numberToShiftDown, cube.getCurrentColumn());
            }
        }
        cubes.removeAll(cubesToRemove);
        initVertices();
    }

    public void addRowToBottom() {
        Cube cube;
        for (int i = 0; i < cubes.size(); i++) {
            cube = cubes.get(i);
            cube.updateCube(cube.getCurrentRow() + 1, cube.getCurrentColumn());
        }
        initVertices();
        int emptyCubeNumber = Utils.getRandomIntInRange(0, Constants.BOXES_PER_ROW - 1);
        ArrayList<Cube> cubesToAdd = new ArrayList<>();
        for (int i = 0; i < Constants.BOXES_PER_ROW; i++) {
            if (i == emptyCubeNumber)
                continue;
            cube = new Cube(getContext(), getCameraController(), 0, i);
            cubesToAdd.add(cube);
        }
        addCubes(cubesToAdd);
    }

    private int getNumberToShiftDown(ArrayList<Integer> rowsToRemove, Cube cube) {
        int result = 0;
        for (Integer row : rowsToRemove) {
            if (cube.getCurrentRow() > row)
                result++;
        }
        return result;
    }

    private void initVertices() {
        Cube cube;
        float[] vertices;
        int index = 0;
        int baseIndex;
        Arrays.fill(points, 0);
        for (int i = 0; i < cubes.size(); i++) {
            cube = cubes.get(i);
            vertices = cube.getVertices();
            baseIndex = index * vertices.length;
            System.arraycopy(vertices, 0, points, baseIndex, vertices.length);
            index++;
        }
        initData(points, POSITION_COUNT);
    }

}
