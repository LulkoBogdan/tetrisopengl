package com.opengl.bogdan_liulko.tetrisopengl.interfaces;

/**
 * Created by bogdan_liulko on 17.03.16.
 */
public interface IRedrawCallback {
    void redraw();
}
