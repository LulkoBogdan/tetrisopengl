package com.opengl.bogdan_liulko.tetrisopengl.interfaces;

import com.opengl.bogdan_liulko.tetrisopengl.shapes.Boost;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.Feature;

public interface IFeatureListener {
    void onFeatureEnable(Feature feature);
    void onFeatureExpired(Boost boost);
}
