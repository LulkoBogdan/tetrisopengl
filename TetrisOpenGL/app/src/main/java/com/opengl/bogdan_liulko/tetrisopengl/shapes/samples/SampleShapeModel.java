package com.opengl.bogdan_liulko.tetrisopengl.shapes.samples;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import com.opengl.bogdan_liulko.tetrisopengl.R;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.BaseShape;
import com.opengl.bogdan_liulko.tetrisopengl.utils.ShaderUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by bogdan_liulko on 03.03.16.
 */
public class SampleShapeModel extends BaseShape {

    private final static int POSITION_COUNT = 3;
    private final static long TIME = 10000L;

    private Context context;
    private FloatBuffer vertexData;

    private int aPositionLocation;
    private int uColorLocation;
    private int uMatrixLocation;
    private int programId;

    private float[] mProjectionMatrix = new float[16];
    private float[] mViewMatrix = new float[16];
    private float[] mModelMatrix = new float[16];
    private float[] mMatrix = new float[16];

    private float[] vertices = {
            -1f, -0.5f, 0.5f,
            1f, -0.5f, 0.5f,
            0, 0.5f, 0.5f,

            -3f, 0, 0,
            3f, 0, 0,

            0, -3f, 0,
            0, 3f, 0,

            0, 0, -3f,
            0, 0, 3f
    };

    public SampleShapeModel(Context context) {
        this.context = context;
        vertexData = ByteBuffer
                .allocateDirect(vertices.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        vertexData.put(vertices);
        int vertexShaderId = ShaderUtils.createShader(context, GLES20.GL_VERTEX_SHADER, R.raw.vertex_shader_3d);
        int fragmentShaderId = ShaderUtils.createShader(context, GLES20.GL_FRAGMENT_SHADER, R.raw.fragment_shader);
        programId = ShaderUtils.createProgram(vertexShaderId, fragmentShaderId);
        GLES20.glUseProgram(programId);
//        createViewMatrix();
        setMove(0, 0);
        prepareData();
    }

    @Override
    public void bindMatrix(int width, int height) {
        float ration;
        float left = -1;
        float right = 1;
        float bottom = -1;
        float top = 1;
        float near = 2;
        float far = 12;
        if (width > height) {
            ration = ((float) width) / ((float) height);
            left *= ration;
            right *= ration;
        } else {
            ration = ((float) height) / ((float) width);
            top *= ration;
            bottom *= ration;
        }
        Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
        bindMatrix();
    }

    @Override
    public void draw() {
        drawAxes();
        drawTriangle();
    }

    private void createViewMatrix() {
        float eyeX = 2;
        float eyeY = 2;
        float eyeZ = 2;

        float centerX = 0;
        float centerY = 0;
        float centerZ = 0;

        float upX = 0;
        float upY = 1;
        float upZ = 0;

        Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
    }

    private void prepareData() {
        aPositionLocation = GLES20.glGetAttribLocation(programId, "a_Position");
        vertexData.position(0);
        GLES20.glVertexAttribPointer(aPositionLocation, POSITION_COUNT, GLES20.GL_FLOAT, false, 0, vertexData);
        GLES20.glEnableVertexAttribArray(aPositionLocation);
        uColorLocation = GLES20.glGetUniformLocation(programId, "u_Color");
        uMatrixLocation = GLES20.glGetUniformLocation(programId, "u_Matrix");
    }

    private void bindMatrix() {
        Matrix.multiplyMM(mMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);
        Matrix.multiplyMM(mMatrix, 0, mProjectionMatrix, 0, mMatrix, 0);
        GLES20.glUniformMatrix4fv(uMatrixLocation, 1, false, mMatrix, 0);
    }

    private void drawAxes() {
        Matrix.setIdentityM(mModelMatrix, 0);
        bindMatrix();

        GLES20.glLineWidth(3);
        GLES20.glUniform4f(uColorLocation, 1.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_LINES, 3, 2);

        GLES20.glUniform4f(uColorLocation, 0.0f, 0.0f, 1.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_LINES, 5, 2);

        GLES20.glUniform4f(uColorLocation, 1.0f, 1.0f, 0.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_LINES, 7, 2);
    }

    private void drawTriangle() {
        Matrix.setIdentityM(mModelMatrix, 0);
        setModelMatrix();
        bindMatrix();
        GLES20.glUniform4f(uColorLocation, 0.0f, 1.0f, 0.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 3);
    }

    private void setModelMatrix() {
        float angle = (float) (System.currentTimeMillis() % TIME) / TIME * 360;
        Matrix.rotateM(mModelMatrix, 0, angle, 0, 1, 1);

//        Matrix.translateM(mModelMatrix, 0, 1, 0, 0);
//        Matrix.scaleM(mModelMatrix, 0, 2, 4, 2);
//        Matrix.rotateM(mModelMatrix, 0, 90 , 0, 0, 1);
    }

    private float currentAngleX;
    private float currentAngleY;

    @Override
    public void setMove(float dx, float dy) {
        Log.d("QWERTY", "dx = " + dx + "   dy = " + dy);
        /*float accurate = 0.05f;
        if (dx > accurate)
            dx = accurate;
        else if (dx < -accurate)
            dx = -accurate;
        if (dy > accurate)
            dy = accurate;
        else if (dy < -accurate)
            dy = -accurate;*/
        dx /= 10f;
        dy /= 10f;
//        float angleX = dx * 2 * 3.1415926f;
//        float angleY = dy * 2 * 3.1415926f;
//        currentAngleX += angleX;
//        currentAngleY += angleY;
//        Log.d("QWERTY", "angleX = " + angleX + "   angleY = " + angleY);
        currentAngleY += dy;
        currentAngleX += dx;
        /*if (currentAngleY > Math.PI / 2f)
            currentAngleY = (float) Math.PI / 2f;
        else if (currentAngleY < -Math.PI / 2f)
            currentAngleY = (float) -Math.PI / 2f;*/
        float eyeX = (float) (Math.cos(currentAngleX) * 2);
        float eyeY = (float) (Math.cos(currentAngleY) * 2);
        float eyeZ = (float) (Math.sin(currentAngleX) * 2);
//        float eyeX = (float) (Math.sin(currentAngleY) * 2f * Math.cos(currentAngleX));
//        float eyeY = (float) (Math.sin(currentAngleY) * 2f * Math.sin(currentAngleX));
//        float eyeZ = (float) (Math.cos(currentAngleY) * 2f);

        float centerX = 0;
        float centerY = 0;
        float centerZ = 0;

        float upX = 0;
        float upY = 1;
        float upZ = 0;

        Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
    }
}
