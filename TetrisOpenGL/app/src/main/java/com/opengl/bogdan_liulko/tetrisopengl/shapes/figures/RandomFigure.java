package com.opengl.bogdan_liulko.tetrisopengl.shapes.figures;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.opengl.bogdan_liulko.tetrisopengl.R;
import com.opengl.bogdan_liulko.tetrisopengl.enums.EAction;
import com.opengl.bogdan_liulko.tetrisopengl.enums.EFigure;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.ILandingListener;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.AShape;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.Cube;
import com.opengl.bogdan_liulko.tetrisopengl.utils.CameraController;
import com.opengl.bogdan_liulko.tetrisopengl.utils.Constants;
import com.opengl.bogdan_liulko.tetrisopengl.utils.GridController;
import com.opengl.bogdan_liulko.tetrisopengl.utils.ShaderUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;

public class RandomFigure extends AFigure {

    private final static int POSITION_COUNT = 3;

    private Cube[][] cubeGrid;

    private int uColorLocation;
    private int uMatrixLocation;
    private int aPositionLocation;
    private int programId;
    private float[] vertices;
    private float[] resultMatrix = new float[16];
    private float[] mModelMatrix = new float[16];
    private FloatBuffer vertexData;

    public RandomFigure(Context context, CameraController cameraController, ArrayList<AShape> shapes, ILandingListener landingListener, Cube[][] cubeGrid, PlacedFigure placedFigure) {
        super(context, cameraController, shapes, landingListener, placedFigure);
        this.cubeGrid = cubeGrid;
        resetFigure();
        initShadow();
    }

    @Override
    public void drawShadow() {
        if (programId == 0 || aPositionLocation ==  0 || uColorLocation == 0 || uMatrixLocation == 0)
            bindDataShadow();
        GLES20.glUseProgram(programId);
        setPositionShadow();
        Matrix.setIdentityM(mModelMatrix, 0);
        bindMatrix();
        GLES20.glUniform4f(uColorLocation, 1, 1, 1, 0.2f);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertices.length / POSITION_COUNT);
    }

    @Override
    public void bindMatrix(int width, int height) {
        super.bindMatrix(width, height);
        bindMatrix();
    }

    private void bindMatrix() {
        GLES20.glUseProgram(programId);
        Matrix.multiplyMM(resultMatrix, 0, getCameraMatrix(), 0, mModelMatrix, 0);
        Matrix.multiplyMM(resultMatrix, 0, getProjectionMatrix(), 0, resultMatrix, 0);
        GLES20.glUniformMatrix4fv(uMatrixLocation, 1, false, resultMatrix, 0);
    }

    private void bindDataShadow() {
        int vertexShaderId = ShaderUtils.createShader(getContext(), GLES20.GL_VERTEX_SHADER, R.raw.vertex_shader_axes);
        int fragmentShaderId = ShaderUtils.createShader(getContext(), GLES20.GL_FRAGMENT_SHADER, R.raw.fragment_shader_axes);
        programId = ShaderUtils.createProgram(vertexShaderId, fragmentShaderId);
        GLES20.glUseProgram(programId);
        aPositionLocation = GLES20.glGetAttribLocation(programId, "a_Position");
        vertexData.position(0);
        setPositionShadow();
        uColorLocation = GLES20.glGetUniformLocation(programId, "u_Color");
        uMatrixLocation = GLES20.glGetUniformLocation(programId, "u_Matrix");
    }

    private void setPositionShadow() {
        GLES20.glVertexAttribPointer(aPositionLocation, POSITION_COUNT, GLES20.GL_FLOAT, false, 0, vertexData);
        GLES20.glEnableVertexAttribArray(aPositionLocation);
    }

    public void initShadow() {
        HashMap<Integer, Cube> shadowPositions = new HashMap<>();
        Cube c;
        for (Cube cube : figureCubes) {
            if ((c = shadowPositions.get(cube.getCurrentColumn())) == null || c.getCurrentRow() > cube.getCurrentRow())
                shadowPositions.put(cube.getCurrentColumn(), cube);
        }
        if (shadowPositions.isEmpty())
            return;
        vertices = GridController.getShadowCoordinates(shadowPositions, cubeGrid);
        vertexData = ByteBuffer
                .allocateDirect(vertices.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        vertexData.put(vertices);
    }

    @Override
    public void move() {
        if (!checkLanding()) {
            for (Cube cube : figureCubes) {
                cube.moveTo(cube.getCurrentRow() - 1, cube.getCurrentColumn());
            }
            moveDown();
        }
    }

    @Override
    public void move(EAction action) {
        switch (action) {
            case MOVE_LEFT:
                moveFigureLeft();
                initShadow();
                break;
            case MOVE_RIGHT:
                moveFigureRight();
                initShadow();
                break;
            case DROP:
                dropFigure();
                break;
            case ROTATE:
                rotate();
                break;
        }
    }

    @Override
    public void rotate() {
        if (figure == EFigure.SQUARE)
            return;
        Cube mainCube = null;
        for (Cube cube : figureCubes) {
            if (cube.isMain()) {
                mainCube = cube;
                continue;
            }
            if (mainCube == null) {
                mainCube = getMainCube();
            }
            if (!checkRotatePlace(cube, mainCube))
                return;
        }
        Cube cube;
        for (int i = 0; i < figureCubes.size(); i++) {
            cube = figureCubes.get(i);
            if (cube.isMain())
                continue;
            rotateCube(cube, mainCube);
        }
        initVertices();
        initShadow();
    }

    private Cube getMainCube() {
        Cube cube;
        for (int i = 0; i < figureCubes.size(); i++) {
            cube = figureCubes.get(i);
            if (cube.isMain())
                return cube;
        }
        return null;
    }

    private boolean checkLanding() {
        for (Cube cube : figureCubes) {
            if (cube.getCurrentRow() == 0 || (cubeGrid[cube.getCurrentColumn()][cube.getCurrentRow() - 1] != null && !figureCubes.contains(cubeGrid[cube.getCurrentColumn()][cube.getCurrentRow() - 1]))) {
                landingListener.figureLanded();
                return true;
            }
        }
        return false;
    }

    private void moveFigureLeft() {
        for (Cube cube : figureCubes) {
            if (cube.getCurrentColumn() == 0)
                return;
            if (cubeGrid[cube.getCurrentColumn() - 1][cube.getCurrentRow()] != null && !figureCubes.contains(cubeGrid[cube.getCurrentColumn() - 1][cube.getCurrentRow()]))
                return;
        }
        for (Cube cube : figureCubes) {
            cube.moveTo(cube.getCurrentRow(), cube.getCurrentColumn() - 1);
        }
        moveLeft();
    }

    private void moveFigureRight() {
        for (Cube cube : figureCubes) {
            if (cube.getCurrentColumn() == Constants.BOXES_PER_ROW - 1)
                return;
            if (cubeGrid[cube.getCurrentColumn() + 1][cube.getCurrentRow()] != null && !figureCubes.contains(cubeGrid[cube.getCurrentColumn() + 1][cube.getCurrentRow()]))
                return;
        }
        for (Cube cube : figureCubes) {
            cube.moveTo(cube.getCurrentRow(), cube.getCurrentColumn() + 1);
        }
        moveRight();
    }

    private void dropFigure() {
        int dRow = checkPlaceToDrop();
        for (Cube cube : figureCubes) {
            cube.moveTo(cube.getCurrentRow() - dRow, cube.getCurrentColumn());
        }
        dropDown(dRow);
        checkLanding();
    }

    private int checkPlaceToDrop() {
        int minDiff = Constants.BOXES_PER_COLUMN - 1;
        int column;
        for (Cube cube : figureCubes) {
            column = cube.getCurrentColumn();
            minDiff = Math.min(cube.getCurrentRow(), minDiff);
            for (int i = cube.getCurrentRow() - 1; i >= 0; i--) {
                if (cubeGrid[column][i] != null) {
                    minDiff = Math.min(minDiff, cube.getCurrentRow() - i - 1);
                }
            }
        }
        return minDiff;
    }

    private void rotateCube(Cube cubeToRotate, Cube centerCube) {
        int column = cubeToRotate.getCurrentColumn();
        int row = cubeToRotate.getCurrentRow();
        if (cubeToRotate.getCurrentColumn() != centerCube.getCurrentColumn() && cubeToRotate.getCurrentRow() != centerCube.getCurrentRow()) {
            if (cubeToRotate.getCurrentColumn() > centerCube.getCurrentColumn()) {
                if (cubeToRotate.getCurrentRow() > centerCube.getCurrentRow()) {
                    row = centerCube.getCurrentRow() - Math.abs(cubeToRotate.getCurrentRow() - centerCube.getCurrentRow());
                } else {
                    column = centerCube.getCurrentColumn() - Math.abs(cubeToRotate.getCurrentColumn() - centerCube.getCurrentColumn());
                }
            } else {
                if (cubeToRotate.getCurrentRow() < centerCube.getCurrentRow()) {
                    row = centerCube.getCurrentRow() + Math.abs(cubeToRotate.getCurrentRow() - centerCube.getCurrentRow());
                } else {
                    column = centerCube.getCurrentColumn() + Math.abs(cubeToRotate.getCurrentColumn() - centerCube.getCurrentColumn());
                }
            }
        } else if (cubeToRotate.getCurrentColumn() == centerCube.getCurrentColumn()) {
            column = centerCube.getCurrentColumn() + (cubeToRotate.getCurrentRow() - centerCube.getCurrentRow());
            row = centerCube.getCurrentRow();
        } else {
            column = centerCube.getCurrentColumn();
            row = centerCube.getCurrentRow() + (centerCube.getCurrentColumn() - cubeToRotate.getCurrentColumn());
        }
        cubeToRotate.rotateCube(row, column, deltaY, deltaX);
    }

    private boolean checkRotatePlace(Cube cubeToRotate, Cube centerCube) {
        int column = cubeToRotate.getCurrentColumn();
        int row = cubeToRotate.getCurrentRow();
        if (cubeToRotate.getCurrentColumn() != centerCube.getCurrentColumn() && cubeToRotate.getCurrentRow() != centerCube.getCurrentRow()) {
            if (cubeToRotate.getCurrentColumn() > centerCube.getCurrentColumn()) {
                if (cubeToRotate.getCurrentRow() > centerCube.getCurrentRow()) {
                    row = centerCube.getCurrentRow() - Math.abs(cubeToRotate.getCurrentRow() - centerCube.getCurrentRow());
                } else {
                    column = centerCube.getCurrentColumn() - Math.abs(cubeToRotate.getCurrentColumn() - centerCube.getCurrentColumn());
                }
            } else {
                if (cubeToRotate.getCurrentRow() < centerCube.getCurrentRow()) {
                    row = centerCube.getCurrentRow() + Math.abs(cubeToRotate.getCurrentRow() - centerCube.getCurrentRow());
                } else {
                    column = centerCube.getCurrentColumn() + Math.abs(cubeToRotate.getCurrentColumn() - centerCube.getCurrentColumn());
                }
            }
        } else if (cubeToRotate.getCurrentColumn() == centerCube.getCurrentColumn()) {
            column = centerCube.getCurrentColumn() + (cubeToRotate.getCurrentRow() - centerCube.getCurrentRow());
            row = centerCube.getCurrentRow();
        } else {
            column = centerCube.getCurrentColumn();
            row = centerCube.getCurrentRow() + (centerCube.getCurrentColumn() - cubeToRotate.getCurrentColumn());
        }
        return column >= 0 && row >= 0 && column < Constants.BOXES_PER_ROW && row < Constants.BOXES_PER_COLUMN &&
                (cubeGrid[column][row] == null || figureCubes.contains(cubeGrid[column][row]));
    }

    private void initVertices() {
        Cube cube;
        float[] vertices;
        int index = 0;
        int baseIndex;
        float[] points = new float[6 * 6 * 3 * 4];
        for (int i = 0; i < figureCubes.size(); i++) {
            cube = figureCubes.get(i);
            vertices = cube.getVertices();
            baseIndex = index * vertices.length;
            System.arraycopy(vertices, 0, points, baseIndex, vertices.length);
            index++;
        }
        initData(points, POSITION_COUNT);
    }

}
