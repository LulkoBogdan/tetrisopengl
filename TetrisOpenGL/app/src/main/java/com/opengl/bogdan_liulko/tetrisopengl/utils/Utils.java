package com.opengl.bogdan_liulko.tetrisopengl.utils;

import java.util.Random;

public class Utils {

    public static int getRandomIntInRange(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }
}
