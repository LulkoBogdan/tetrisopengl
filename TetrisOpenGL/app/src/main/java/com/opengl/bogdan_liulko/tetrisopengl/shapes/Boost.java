package com.opengl.bogdan_liulko.tetrisopengl.shapes;

import android.content.Context;

import com.opengl.bogdan_liulko.tetrisopengl.enums.EFeature;
import com.opengl.bogdan_liulko.tetrisopengl.game.GameTimer;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.IFeatureListener;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.ITickListener;
import com.opengl.bogdan_liulko.tetrisopengl.utils.CameraController;

public class Boost extends Feature implements ITickListener {

    private final static int TOP_ROW = 9;
    private final static int POSITION_COUNT = 3;
    private final static int START_TIME_TO_LIVE = 10000;//10 seconds

    private int timeToLive;

    private IFeatureListener featureListener;

    public Boost(Context context, CameraController cameraController, EFeature feature, IFeatureListener featureListener) {
        super(context, cameraController, feature, getRow(feature), -4, 0);
        this.featureListener = featureListener;
        timeToLive = START_TIME_TO_LIVE;
    }

    @Override
    public void onTick() {
        timeToLive -= GameTimer.ITERATION;
        checkTimeToLive();
    }

    private void checkTimeToLive() {
        if (timeToLive <= 0)
            featureListener.onFeatureExpired(this);
    }

    public void increaseTime() {
        timeToLive += START_TIME_TO_LIVE;
    }

    @Override
    protected int getPositionCount() {
        return POSITION_COUNT;
    }

    private static int getRow(EFeature feature) {
        EFeature[] features = EFeature.values();
        int position = 0;
        for (EFeature f : features) {
            if (feature == f)
                break;
            if (f.isTemporary())
                position += 2;
        }
        return TOP_ROW - position;
    }
}
