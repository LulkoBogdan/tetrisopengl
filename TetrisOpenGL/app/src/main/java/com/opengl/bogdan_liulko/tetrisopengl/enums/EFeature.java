package com.opengl.bogdan_liulko.tetrisopengl.enums;

import com.opengl.bogdan_liulko.tetrisopengl.R;

public enum EFeature {
    SLOW(true, false), SPEED_UP(false, true), FREEZE(true, true), ADD_ROW(false, false), REMOVE_ROW(true, false), INVERSE(false, true);

    private boolean support;
    private boolean temporary;

    EFeature(boolean support, boolean temporary) {
        this.support = support;
        this.temporary = temporary;
    }

    public static EFeature getFeatureById(int id) {
        EFeature[] features = values();
        if (id < 0 || id >= features.length)
            return null;
        return features[id];
    }

    public boolean isSupport() {
        return support;
    }

    public boolean isTemporary() {
        return temporary;
    }

    public int getTextureId() {
        switch (this) {
            case SLOW:
                return R.drawable.ic_slower;
            case SPEED_UP:
                return R.drawable.ic_faster;
            case FREEZE:
                return R.drawable.ic_freeze;
            case ADD_ROW:
                return R.drawable.ic_add_row;
            case REMOVE_ROW:
                return R.drawable.ic_remove_row;
            default:
                return R.drawable.ic_inverse;

        }
    }
}
