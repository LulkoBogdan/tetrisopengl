package com.opengl.bogdan_liulko.tetrisopengl.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.RelativeLayout;

public class StartDialog extends RelativeLayout implements Animation.AnimationListener{

    private final static long ANIMATION_DURATION = 300;

    private RelativeLayout rlDarkBackground;
    private RelativeLayout rlMenu;
    private Button btnResumeGame;

    private Animation animationShowDarkBackground;
    private Animation animationHideDarkBackground;
    private Animation animationShowMenuContent;
    private Animation animationHideMenuContent;

    public StartDialog(Context context) {
        super(context);
        initAnimations();
    }

    public StartDialog(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAnimations();
    }

    public StartDialog(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAnimations();
    }

    public StartDialog(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initAnimations();
    }

    public void showMenu(boolean resume) {
        btnResumeGame.setVisibility(resume ? VISIBLE : GONE);
        setVisibility(VISIBLE);
        animationShowMenuContent();
        animateShowDarkBackground();
    }

    public void hideMenu() {
        animationHideMenuContent();
        animationHideDarkBackground();
    }

    public void setViews(RelativeLayout rlDarkBackground, RelativeLayout rlMenu, Button btnResumeGame) {
        this.rlDarkBackground = rlDarkBackground;
        this.rlMenu = rlMenu;
        this.btnResumeGame = btnResumeGame;
    }

    private void animateShowDarkBackground() {
        rlDarkBackground.setAnimation(animationShowDarkBackground);
        rlDarkBackground.startAnimation(animationShowDarkBackground);
    }

    private void animationHideDarkBackground() {
        rlDarkBackground.setAnimation(animationHideDarkBackground);
        rlDarkBackground.startAnimation(animationHideDarkBackground);
    }

    private void animationShowMenuContent() {
        rlMenu.setAnimation(animationShowMenuContent);
        rlMenu.startAnimation(animationShowMenuContent);
    }

    private void animationHideMenuContent() {
        rlMenu.setAnimation(animationHideMenuContent);
        rlMenu.startAnimation(animationHideMenuContent);
    }

    private void initAnimations() {
        animationShowDarkBackground = new AlphaAnimation(0, 1);
        animationShowDarkBackground.setDuration(ANIMATION_DURATION);
        animationHideDarkBackground = new AlphaAnimation(1, 0);
        animationHideDarkBackground.setDuration(ANIMATION_DURATION);
        animationHideDarkBackground.setAnimationListener(this);

        animationShowMenuContent = new ScaleAnimation(0, 1, 0, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animationShowMenuContent.setDuration(ANIMATION_DURATION);
        animationHideMenuContent = new ScaleAnimation(1, 0, 1, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animationHideMenuContent.setDuration(ANIMATION_DURATION);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        setVisibility(GONE);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
