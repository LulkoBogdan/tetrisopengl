package com.opengl.bogdan_liulko.tetrisopengl.interfaces;

import com.opengl.bogdan_liulko.tetrisopengl.shapes.Boost;

public interface IScoreChangeListener {
    int onLinesRemoved(int countOfLines);
    void onGameReset();
    void onFieldClear();
    void onFeatureEnable(Boost boost);
}
