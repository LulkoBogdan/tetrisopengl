package com.opengl.bogdan_liulko.tetrisopengl.shapes.samples;

import android.content.Context;
import android.opengl.GLES20;

import com.opengl.bogdan_liulko.tetrisopengl.R;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.BaseShape;
import com.opengl.bogdan_liulko.tetrisopengl.utils.ShaderUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by bogdan_liulko on 01.03.16.
 */
public class SampleShape2 extends BaseShape {
    private Context context;
    private FloatBuffer vertexData;

    private int aColorLocation;
    private int aPositionLocation;

    private float[] vertices = {
            -0.4f, 0.6f, 1.0f, 0.0f, 0.0f,
            0.4f, 0.6f, 0.0f, 1.0f, 0.0f,

            0.6f, 0.4f, 0.0f, 0.0f, 0.1f,
            0.6f, -0.4f, 1.0f, 1.0f, 1.0f,

            0.4f, -0.6f, 1.0f, 1.0f, 0.0f,
            -0.4f, -0.6f, 1.0f, 0.0f, 1.0f
    };

    public SampleShape2(Context context) {
        this.context = context;
        vertexData = ByteBuffer
                .allocateDirect(vertices.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        vertexData.put(vertices);
    }

    @Override
    public void draw() {
        int vertexShaderId = ShaderUtils.createShader(context, GLES20.GL_VERTEX_SHADER, R.raw.vertex_shader_2);
        int fragmentShaderId = ShaderUtils.createShader(context, GLES20.GL_FRAGMENT_SHADER, R.raw.fragment_shader_2);
        int programId = ShaderUtils.createProgram(vertexShaderId, fragmentShaderId);
        GLES20.glUseProgram(programId);

        aPositionLocation = GLES20.glGetAttribLocation(programId, "a_Position");
        vertexData.position(0);
        GLES20.glVertexAttribPointer(aPositionLocation, 2, GLES20.GL_FLOAT, false, 20, vertexData);
        GLES20.glEnableVertexAttribArray(aPositionLocation);

        aColorLocation = GLES20.glGetAttribLocation(programId, "a_Color");
        vertexData.position(2);
        GLES20.glVertexAttribPointer(aColorLocation, 3, GLES20.GL_FLOAT, false, 20, vertexData);
        GLES20.glEnableVertexAttribArray(aColorLocation);
        GLES20.glDrawArrays(GLES20.GL_LINE_LOOP, 0, 6);
    }
}
