package com.opengl.bogdan_liulko.tetrisopengl.shapes.figures;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.opengl.bogdan_liulko.tetrisopengl.enums.EAction;
import com.opengl.bogdan_liulko.tetrisopengl.enums.EFigure;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.ILandingListener;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.AShape;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.Cube;
import com.opengl.bogdan_liulko.tetrisopengl.utils.CameraController;
import com.opengl.bogdan_liulko.tetrisopengl.utils.Constants;

import java.util.ArrayList;
import java.util.Random;

public abstract class AFigure extends AShape {

    private final static int POSITION_COUNT = 3;

    protected ILandingListener landingListener;
    protected ArrayList<Cube> figureCubes;
    protected EFigure figure;
    protected PlacedFigure placedFigure;

    protected int deltaX;
    protected int deltaY;

    public AFigure(Context context, CameraController cameraController, ArrayList<AShape> shapes, ILandingListener landingListener, PlacedFigure placedFigure) {
        super(context, cameraController);
        this.landingListener = landingListener;
        this.figureCubes = new ArrayList<>();
        this.placedFigure = placedFigure;
        shapes.add(this);
    }

    @Override
    public void draw() {
        drawShadow();
        drawCubes();
    }

    public abstract void drawShadow();
    public abstract void move();
    public abstract void move(EAction action);
    public abstract void rotate();

    public void drawCubes() {
        if (programId == 0 || aPositionLocation ==  0 || uColorLocation == 0 || uMatrixLocation == 0)
            bindData();
        GLES20.glUseProgram(programId);
        setPosition();
        Matrix.setIdentityM(mModelMatrix, 0);
        Matrix.translateM(mModelMatrix, 0, (deltaX) * Constants.BOX_WIDTH, (deltaY) * Constants.BOX_WIDTH, 0);
        bindMatrix();
        GLES20.glLineWidth(Constants.FALLING_CUBE_LINE_WIDTH);
        GLES20.glUniform4f(uColorLocation, Constants.Color.CUBE_LINE_RED, Constants.Color.CUBE_LINE_GREEN, Constants.Color.CUBE_LINE_BLUE, 1);
        GLES20.glDrawArrays(GLES20.GL_LINES, 0, getPointsCount());
        GLES20.glUniform4f(uColorLocation, Constants.Color.FALLING_CUBE_RED, Constants.Color.FALLING_CUBE_GREEN, Constants.Color.FALLING_CUBE_BLUE, 1);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, getPointsCount());
    }

    @Override
    public void bindMatrix(int width, int height) {
        super.bindMatrix(width, height);
        bindMatrix();
    }

    private void bindMatrix() {
        GLES20.glUseProgram(programId);
        Matrix.multiplyMM(resultMatrix, 0, getCameraMatrix(), 0, mModelMatrix, 0);
        Matrix.multiplyMM(resultMatrix, 0, getProjectionMatrix(), 0, resultMatrix, 0);
        GLES20.glUniformMatrix4fv(uMatrixLocation, 1, false, resultMatrix, 0);
    }

    private int getRandomFigureId() {
        int max = EFigure.values().length - 1;
        int min = 0;
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    private void addCubes() {
        figureCubes.clear();
        initData(figure.addCubes(figureCubes, getContext(), getCameraController()), POSITION_COUNT);
    }

    public void resetFigure(EFigure figure) {
        deltaX = 0;
        deltaY = 0;
        this.figure = figure;
        placedFigure.addCubes(figureCubes);
        addCubes();
    }

    public void resetFigure() {
        resetFigure(EFigure.getFigureById(getRandomFigureId()));
    }

    public void clearFigure() {
        figureCubes.clear();
        resetFigure();
    }

    protected void moveLeft() {
        deltaX--;
    }

    protected void moveRight() {
        deltaX++;
    }

    protected void moveDown() {
        deltaY--;
    }

    protected void dropDown(int delta) {
        deltaY -= delta;
    }

    @Override
    protected int getPositionCount() {
        return POSITION_COUNT;
    }
}
