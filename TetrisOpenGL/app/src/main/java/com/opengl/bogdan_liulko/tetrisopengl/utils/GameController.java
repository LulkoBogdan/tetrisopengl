package com.opengl.bogdan_liulko.tetrisopengl.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Vibrator;

import com.opengl.bogdan_liulko.tetrisopengl.enums.EAction;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.Boost;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.Feature;
import com.opengl.bogdan_liulko.tetrisopengl.game.GameTimer;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.IFeatureListener;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.ILandingListener;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.IRedrawCallback;
import com.opengl.bogdan_liulko.tetrisopengl.interfaces.ITimerListener;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.AShape;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.Cube;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.figures.NextFigure;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.figures.PlacedFigure;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.figures.RandomFigure;
import com.opengl.bogdan_liulko.tetrisopengl.ui.views.StartDialog;

import java.util.ArrayList;
import java.util.HashMap;

public class GameController implements ITimerListener, ILandingListener, IFeatureListener{

    private final static int FIGURE_COUNT_TO_SPEED_UP = 15;

    private final ScoreController scoreController;
    private final StartDialog startDialog;
    private final Activity activity;
    private final Vibrator vibrator;
    private final FeaturesController featuresController;
    private final CameraController cameraController;
    private final ArrayList<AShape> shapes;

    private RandomFigure figure;
    private NextFigure nextFigure;
    private PlacedFigure placedFigure;
    private GameTimer gameTimer;
    private IRedrawCallback redrawCallback;
    private Cube[][] cubeGrid;
    private ArrayList<Boost> activeBoosts;

    private boolean running;
    private boolean freeze;
    private boolean inverse;
    private int countOfLandedFigures;

    public GameController(Context context, CameraController cameraController, ArrayList<AShape> shapes, IRedrawCallback redrawCallback, ScoreController scoreController, StartDialog startDialog) {
        this.redrawCallback = redrawCallback;
        this.scoreController = scoreController;
        this.startDialog = startDialog;
        this.activity = (Activity) context;
        this.cameraController = cameraController;
        this.shapes = shapes;
        cubeGrid = new Cube[Constants.BOXES_PER_ROW][Constants.BOXES_PER_COLUMN];
        placedFigure = new PlacedFigure(context, cameraController, shapes);
        figure = new RandomFigure(context, cameraController, shapes, this, cubeGrid, placedFigure);
        nextFigure = new NextFigure(context, cameraController, shapes);
        featuresController = new FeaturesController(context, cameraController, shapes, placedFigure, this, redrawCallback);
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        activeBoosts = new ArrayList<>();
    }

    public void startGame() {
        placedFigure.clearCubes();
        figure.clearFigure();
        GridController.getCubeGrid(placedFigure.getPlacedCubes(), cubeGrid);
        figure.initShadow();
        redrawCallback.redraw();
        scoreController.onGameReset();
        featuresController.resetCounter();
        shapes.removeAll(activeBoosts);
        activeBoosts.clear();

        countOfLandedFigures = 0;
        if (gameTimer == null) {
            gameTimer = new GameTimer(this);
            gameTimer.addTickListener(featuresController);
            gameTimer.startCounting();
        } else {
            gameTimer.resumeCounting();
            gameTimer.resetPeriod();
        }
        running = true;
    }

    public void stopGame() {
        if (!running)
            return;
        gameTimer.clearTickListeners();
        gameTimer.stopCounting();
        gameTimer = null;
        running = false;
    }

    public void pauseGame() {
        if (!running) {
            gameTimer.resumeCounting();
            running = true;
        } else {
            gameTimer.pauseCounting();
            running = false;
        }
    }

    public void setAction(EAction action) {
        if (!running)
            return;
        if (inverse)
            action = action.getInverseAction();
        figure.move(action);
        if (action == EAction.ROTATE)
            GridController.getCubeGrid(placedFigure.getPlacedCubes(), cubeGrid);
    }

    @Override
    public void onTime() {
        if (freeze)
            return;
        figure.move();
        redrawCallback.redraw();
    }

    @Override
    public void figureLanded() {
        figure.resetFigure(nextFigure.getNextFigure());
        nextFigure.resetFigure();
        HashMap<Integer, Integer> filledRows = new HashMap<>();
        if (GridController.getCubeGrid(placedFigure.getPlacedCubes(), cubeGrid, filledRows)) {
            showStartDialog();
            if (vibrator.hasVibrator())
                vibrator.vibrate(400);
            gameTimer.pauseCounting();
            running = false;
        } else {
            checkFullLines(filledRows);
            countOfLandedFigures++;
            if (countOfLandedFigures > FIGURE_COUNT_TO_SPEED_UP) {
                countOfLandedFigures = 0;
                gameTimer.speedUp();
            }
        }
        figure.initShadow();
    }

    private void checkFullLines(HashMap<Integer, Integer> filledRows) {
        ArrayList<Integer> rowsToRemove = new ArrayList<>();
        int notEmptyRowsCount = 0;
        for (int i = 0; i < filledRows.size(); i++) {
            if (filledRows.get(i) == Constants.BOXES_PER_ROW)
                rowsToRemove.add(i);
            if (filledRows.get(i) > 0)
                notEmptyRowsCount++;
        }
        int points = scoreController.onLinesRemoved(rowsToRemove.size());
        featuresController.addPoints(points);
        if (rowsToRemove.isEmpty())
            return;
        if (notEmptyRowsCount == rowsToRemove.size()) {
            scoreController.onFieldClear();
        }
        placedFigure.removeRows(rowsToRemove);
        featuresController.rowsRemoved(rowsToRemove);
        GridController.getCubeGrid(placedFigure.getPlacedCubes(), cubeGrid);
    }

    public boolean isRunning() {
        return running;
    }

    private void showStartDialog() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                startDialog.showMenu(false);
            }
        });
    }

    @Override
    public void onFeatureEnable(Feature feature) {
        Boost boost;
        for (int i = 0; i < activeBoosts.size(); i++) {
            boost = activeBoosts.get(i);
            if (feature.getFeature() == boost.getFeature()) {
                boost.increaseTime();
                return;
            }
        }
        boost = new Boost(activity, cameraController, feature.getFeature(), this);
        if (boost.getFeature().isTemporary()) {
            gameTimer.addTickListener(boost);
            activeBoosts.add(boost);
            shapes.add(boost);
        }
        boostAdded(boost);
    }

    @Override
    public void onFeatureExpired(Boost boost) {
        activeBoosts.remove(boost);
        shapes.remove(boost);
        disableBoost(boost);
    }

    private void boostAdded(Boost boost) {
        switch (boost.getFeature()) {
            case ADD_ROW:
                addNewRow();
                break;
            case FREEZE:
                freeze = true;
                break;
            case INVERSE:
                inverse = true;
                break;
            case REMOVE_ROW:
                removeRow();
                break;
            case SLOW:
                gameTimer.speedDown();
                break;
            case SPEED_UP:
                gameTimer.increaseSpeed(true);
                break;
        }
        scoreController.onFeatureEnable(boost);
    }

    private void disableBoost(Boost boost) {
        switch (boost.getFeature()) {
            case FREEZE:
                freeze = false;
                break;
            case INVERSE:
                inverse = false;
                break;
            case SPEED_UP:
                gameTimer.increaseSpeed(false);
                break;
        }
        gameTimer.removeTickListener(boost);
        shapes.remove(boost);
    }

    private void addNewRow() {
        placedFigure.addRowToBottom();
        preparePlacedCubes();
    }

    private void removeRow() {
        ArrayList<Integer> rowsToRemove = new ArrayList<>();
        rowsToRemove.add(0);
        placedFigure.removeRows(rowsToRemove);
        featuresController.rowsRemoved(rowsToRemove);//TODO check
        GridController.getCubeGrid(placedFigure.getPlacedCubes(), cubeGrid);
        figure.initShadow();
    }

    private void preparePlacedCubes() {
        HashMap<Integer, Integer> filledRows = new HashMap<>();
        if (GridController.getCubeGrid(placedFigure.getPlacedCubes(), cubeGrid, filledRows)) {
            showStartDialog();
            if (vibrator.hasVibrator())
                vibrator.vibrate(400);
            gameTimer.pauseCounting();
            running = false;
        }
        figure.initShadow();
    }
}
