package com.opengl.bogdan_liulko.tetrisopengl.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import com.opengl.bogdan_liulko.tetrisopengl.R;

public class TextureUtils {

    private final static int BITMAP_SIZE = 1024;

    public static int loadTexture(Context context, int resourceId) {
        final int[] textureIds = new int[1];
        GLES20.glGenTextures(1, textureIds, 0);
        if (textureIds[0] == 0) {
            return 0;
        }
        Bitmap bitmap = getResourceBitmap(context, resourceId);
        if (bitmap == null) {
            GLES20.glDeleteTextures(1, textureIds, 0);
            return 0;
        }
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureIds[0]);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
        bitmap.recycle();
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
        return textureIds[0];
    }

    public static int getSkyBoxTexture(Context context) {
        final int[] textureIds = new int[1];
        GLES20.glGenTextures(1, textureIds, 0);
        if (textureIds[0] == 0) {
            return 0;
        }
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_CUBE_MAP, textureIds[0]);

        Bitmap sourceBitmap = getResourceBitmap(context, R.drawable.ic_sky_box);
        Bitmap bitmap;
//        bitmap = getFront(sourceBitmap);
        bitmap = getBack(sourceBitmap);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, bitmap, 0);
        bitmap.recycle();

//        bitmap = getBack(sourceBitmap);
        bitmap = getFront(sourceBitmap);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, bitmap, 0);
        bitmap.recycle();

        bitmap = getLeft(sourceBitmap);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, bitmap, 0);
        bitmap.recycle();

        bitmap = getRight(sourceBitmap);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, bitmap, 0);
        bitmap.recycle();

        bitmap = getBottom(sourceBitmap);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, bitmap, 0);
        bitmap.recycle();

        bitmap = getTop(sourceBitmap);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, bitmap, 0);
        bitmap.recycle();

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_CUBE_MAP, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_CUBE_MAP, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
        return textureIds[0];
    }

    private static Bitmap getResourceBitmap(Context context, int resourceId) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        return BitmapFactory.decodeResource(context.getResources(), resourceId, options);
    }

    private static Bitmap getBottom(Bitmap sourceBitmap) {
        return Bitmap.createBitmap(sourceBitmap, BITMAP_SIZE, BITMAP_SIZE * 2, BITMAP_SIZE, BITMAP_SIZE);
    }

    private static Bitmap getTop(Bitmap sourceBitmap) {
        return Bitmap.createBitmap(sourceBitmap, BITMAP_SIZE, 0, BITMAP_SIZE, BITMAP_SIZE);
    }

    private static Bitmap getFront(Bitmap sourceBitmap) {
        return Bitmap.createBitmap(sourceBitmap, BITMAP_SIZE, BITMAP_SIZE, BITMAP_SIZE, BITMAP_SIZE);
    }

    private static Bitmap getBack(Bitmap sourceBitmap) {
        return Bitmap.createBitmap(sourceBitmap, BITMAP_SIZE * 3, BITMAP_SIZE, BITMAP_SIZE, BITMAP_SIZE);
    }

    private static Bitmap getLeft(Bitmap sourceBitmap) {
        return Bitmap.createBitmap(sourceBitmap, 0, BITMAP_SIZE, BITMAP_SIZE, BITMAP_SIZE);
    }

    private static Bitmap getRight(Bitmap sourceBitmap) {
        return Bitmap.createBitmap(sourceBitmap, BITMAP_SIZE * 2, BITMAP_SIZE, BITMAP_SIZE, BITMAP_SIZE);
    }

}
