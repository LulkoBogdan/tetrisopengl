package com.opengl.bogdan_liulko.tetrisopengl.shapes.samples;

import android.content.Context;
import android.opengl.GLES20;

import com.opengl.bogdan_liulko.tetrisopengl.R;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.BaseShape;
import com.opengl.bogdan_liulko.tetrisopengl.utils.ShaderUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by bogdan_liulko on 01.03.16.
 */
public class SampleShape extends BaseShape {

    private Context context;
    private FloatBuffer vertexData;

    private int uColorLocation;
    private int aPositionLocation;

    /*private float[] vertices = {
            -0.9f, 0.8f,
            -0.9f, 0.2f,
            -0.5f, 0.8f,

            -0.6f, 0.2f,
            -0.2f, 0.2f,
            -0.2f, 0.8f,

            0.1f, 0.8f,
            0.1f, 0.2f,
            0.5f, 0.8f,

            0.1f, 0.2f,
            0.5f, 0.2f,
            0.5f, 0.8f
    };*/
    /*private float[] vertices = { //TODO 1
            0.1f, 0.8f,
            0.1f, 0.2f,
            0.5f, 0.8f,

            0.1f, 0.2f,
            0.5f, 0.2f,
            0.5f, 0.8f,
    };*/

    /*private float[] vertices = { //TODO 2
            0.1f, 0.8f,
            0.1f, 0.2f,
            0.5f, 0.8f,
            0.5f, 0.2f,
    };*/

    private float[] vertices = {
            -0.9f, 0.8f,
            -0.9f, 0.2f,
            -0.5f, 0.8f,

            -0.6f, 0.2f,
            -0.2f, 0.2f,
            -0.2f, 0.8f,

            0.1f, 0.8f,
            0.1f, 0.2f,
            0.5f, 0.8f,

            0.1f, 0.2f,
            0.5f, 0.2f,
            0.5f, 0.8f,

            -0.7f, -0.1f,
            0.7f, -0.1f,

            -0.6f, -0.2f,
            0.6f, -0.2f,

            -0.5f, -0.3f,

            0.0f, -0.3f,

            0.5f, -0.3f
    };

    public SampleShape(Context context) {
        this.context = context;
        vertexData = ByteBuffer
                .allocateDirect(vertices.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        vertexData.put(vertices);
    }

    @Override
    public void draw() {
        int vertexShaderId = ShaderUtils.createShader(context, GLES20.GL_VERTEX_SHADER, R.raw.vertex_shader);
        int fragmentShaderId = ShaderUtils.createShader(context, GLES20.GL_FRAGMENT_SHADER, R.raw.fragment_shader);
        int programId = ShaderUtils.createProgram(vertexShaderId, fragmentShaderId);
        GLES20.glUseProgram(programId);
        uColorLocation = GLES20.glGetUniformLocation(programId, "u_Color");

        aPositionLocation = GLES20.glGetAttribLocation(programId, "a_Position");
        vertexData.position(0);
        GLES20.glVertexAttribPointer(aPositionLocation, 2, GLES20.GL_FLOAT, false, 0, vertexData);
        GLES20.glEnableVertexAttribArray(aPositionLocation);
        //GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertices.length / 2); //TODO 1
        //GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, vertices.length / 2); //TODO 2
        GLES20.glLineWidth(5);
        GLES20.glUniform4f(uColorLocation, 0.0f, 0.0f, 1.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 12);
        GLES20.glUniform4f(uColorLocation, 0.0f, 1.0f, 0.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_LINES, 12, 4);
        GLES20.glUniform4f(uColorLocation, 1.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_POINTS, 16, 3);
    }
}