package com.opengl.bogdan_liulko.tetrisopengl.shapes.figures;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.opengl.bogdan_liulko.tetrisopengl.R;
import com.opengl.bogdan_liulko.tetrisopengl.models.VertexArray;
import com.opengl.bogdan_liulko.tetrisopengl.shapes.AShape;
import com.opengl.bogdan_liulko.tetrisopengl.utils.CameraController;
import com.opengl.bogdan_liulko.tetrisopengl.utils.ShaderUtils;
import com.opengl.bogdan_liulko.tetrisopengl.utils.TextureUtils;

import java.nio.ByteBuffer;

public class SkyBox extends AShape{

    private final static int POSITION_COUNT = 3;
    private static final String U_MATRIX = "u_Matrix";
    private static final String U_TEXTURE_UNIT = "u_TextureUnit";
    private static final String A_POSITION = "a_Position";

    private final VertexArray vertexArray;
    private final ByteBuffer indexArray;

    private int uTextureUnitLocation;
    private int textureId;

    public SkyBox(Context context, CameraController cameraController) {
        super(context, cameraController);
        // Create a unit cube.
        vertexArray = new VertexArray(new float[] {
                -1,  1,  1,     // (0) Top-left near
                1,  1,  1,     // (1) Top-right near
                -1, -1,  1,     // (2) Bottom-left near
                1, -1,  1,     // (3) Bottom-right near
                -1,  1, -1,     // (4) Top-left far
                1,  1, -1,     // (5) Top-right far
                -1, -1, -1,     // (6) Bottom-left far
                1, -1, -1      // (7) Bottom-right far
        });

        // 6 indices per cube side
        indexArray =  ByteBuffer.allocateDirect(6 * 6)
                .put(new byte[] {
                        // Front
                        1, 3, 0,
                        0, 3, 2,

                        // Back
                        4, 6, 5,
                        5, 6, 7,

                        // Left
                        0, 2, 4,
                        4, 2, 6,

                        // Right
                        5, 7, 1,
                        1, 7, 3,

                        // Top
                        5, 1, 4,
                        4, 1, 0,

                        // Bottom
                        6, 2, 7,
                        7, 2, 3
                });
        indexArray.position(0);
    }

    @Override
    public void draw() {
        if (programId == 0 || textureId == 0) {
            textureId = TextureUtils.getSkyBoxTexture(getContext());
            getProgramId();
        }
        GLES20.glUseProgram(programId);
        bindData();
//        vertexData.position(0);
//        vertexDataTexture.position(0);
//        setPosition();
        bindMatrix();
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, 36, GLES20.GL_UNSIGNED_BYTE, indexArray);
    }

    @Override
    protected int getPositionCount() {
        return POSITION_COUNT;
    }

    @Override
    protected void bindData() {
        uMatrixLocation = GLES20.glGetUniformLocation(programId, U_MATRIX);
        uTextureUnitLocation = GLES20.glGetUniformLocation(programId, U_TEXTURE_UNIT);
        aPositionLocation = GLES20.glGetAttribLocation(programId, A_POSITION);

        vertexArray.setVertexAttribPointer(0, aPositionLocation, getPositionCount(), 0);
        setUniforms(textureId);
    }

    public void setUniforms(int textureId) {
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_CUBE_MAP, textureId);
        GLES20.glUniform1i(uTextureUnitLocation, 0);
    }

    @Override
    public void bindMatrix(int width, int height) {
//        super.bindMatrix(width, height);
        bindMatrix();
    }

    private void bindMatrix() {
        GLES20.glUseProgram(programId);
        Matrix.multiplyMM(resultMatrix, 0, getProjectionMatrix(), 0, getCameraMatrix(), 0);
        GLES20.glUniformMatrix4fv(uMatrixLocation, 1, false, resultMatrix, 0);
    }

    private void getProgramId() {
        int vertexShaderId = ShaderUtils.createShader(getContext(), GLES20.GL_VERTEX_SHADER, R.raw.vertex_shader_skybox);
        int fragmentShaderId = ShaderUtils.createShader(getContext(), GLES20.GL_FRAGMENT_SHADER, R.raw.fragment_shader_skybox);
        programId = ShaderUtils.createProgram(vertexShaderId, fragmentShaderId);
    }

}
